/*------------------------------------------------
	Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
	See ROP.cu for full notice
------------------------------------------------*/

#include "initialization.h"
#include "multislice.h"
#include "optimization.h"
#include "mathematics.h"
#include "cublas_assert.hpp"
#include <fstream>


__constant__ float c_ax, c_bx, c_cx, c_dx;
__constant__ float c_ay, c_by, c_cy, c_dy;


__global__ void derivative_x_real(cufftComplex *f, cufftComplex *df, int m1, int m2)
{
    extern __shared__ float s_f[]; // 4-wide halo

    int i = threadIdx.x;
    int j = blockIdx.x*blockDim.y + threadIdx.y;
    int si = i + 4;       // local i for shared memory access + halo offset

    int globalIdx = j * m1 + i;

    s_f[si] = f[globalIdx].x;

    __syncthreads();

    // fill in periodic images in shared memory array
    if (i < 4) {
        s_f[si - 4] = s_f[si + m1 - 5];
        s_f[si + m1] = s_f[si + 1];
    }

    __syncthreads();

    df[globalIdx].x =
            (c_ax * (s_f[si + 1] - s_f[si - 1])
             + c_bx * (s_f[si + 2] - s_f[si - 2])
             + c_cx * (s_f[si + 3] - s_f[si - 3])
             + c_dx * (s_f[si + 4] - s_f[si - 4]));
}

__global__ void derivative_x_imag(cufftComplex *f, cufftComplex *df, int m1, int m2)
{
    extern __shared__ float s_f[]; // 4-wide halo

    int i = threadIdx.x;
    int j = blockIdx.x*blockDim.y + threadIdx.y;
    int si = i + 4;       // local i for shared memory access + halo offset

    int globalIdx = j * m1 + i;

    s_f[si] = f[globalIdx].y;

    __syncthreads();

    // fill in periodic images in shared memory array
    if (i < 4) {
        s_f[si - 4] = s_f[si + m1 - 5];
        s_f[si + m1] = s_f[si + 1];
    }

    __syncthreads();

    df[globalIdx].y =
            (c_ax * (s_f[si + 1] - s_f[si - 1])
             + c_bx * (s_f[si + 2] - s_f[si - 2])
             + c_cx * (s_f[si + 3] - s_f[si - 3])
             + c_dx * (s_f[si + 4] - s_f[si - 4]));
}


__global__ void derivative_y_real(cufftComplex *f, cufftComplex *df, int m1, int m2)
{
    extern __shared__ float s_f[];

    int i = blockIdx.x*blockDim.x + threadIdx.x;
    int j = threadIdx.y;
    int sj = j + 4;


    int globalIdx = j * m1 + i;

    s_f[sj] = f[globalIdx].x;

    __syncthreads();

    if (j < 4) {
        s_f[sj - 4] = s_f[sj + m2 - 5];
        s_f[sj + m2] = s_f[sj + 1];
    }
    __syncthreads();

    df[globalIdx].x =
            (c_ay * (s_f[sj + 1] - s_f[sj - 1])
             + c_by * (s_f[sj + 2] - s_f[sj - 2])
             + c_cy * (s_f[sj + 3] - s_f[sj - 3])
             + c_dy * (s_f[sj + 4] - s_f[sj - 4]));
}

__global__ void derivative_y_imag(cufftComplex *f, cufftComplex *df, int m1, int m2)
{
    extern __shared__ float s_f[];

    int i = blockIdx.x*blockDim.x + threadIdx.x;
    int j = threadIdx.y;
    int sj = j + 4;


    int globalIdx = j * m1 + i;

    s_f[sj] = f[globalIdx].y;

    __syncthreads();

    if (j < 4) {
        s_f[sj - 4] = s_f[sj + m2 - 5];
        s_f[sj + m2] = s_f[sj + 1];
    }
    __syncthreads();

    df[globalIdx].y =
            (c_ay * (s_f[sj + 1] - s_f[sj - 1])
             + c_by * (s_f[sj + 2] - s_f[sj - 2])
             + c_cy * (s_f[sj + 3] - s_f[sj - 3])
             + c_dy * (s_f[sj + 4] - s_f[sj - 4]));
}


void probeDerivative(cufftComplex* psi, cufftComplex* dpsi, params_t* params, int dimension)
{

    int m1 = params->IM.ProbeDim;
    int m2 = params->IM.ProbeDim;

    dim3 grid[2], block[2];

    float ax = 4.f / 5.f;
    float bx = -1.f / 5.f;
    float cx = 4.f / 105.f;
    float dx = -1.f / 280.f;
    cuda_assert(cudaMemcpyToSymbol(c_ax, &ax, sizeof(float), 0, cudaMemcpyHostToDevice));
    cuda_assert(cudaMemcpyToSymbol(c_bx, &bx, sizeof(float), 0, cudaMemcpyHostToDevice));
    cuda_assert(cudaMemcpyToSymbol(c_cx, &cx, sizeof(float), 0, cudaMemcpyHostToDevice));
    cuda_assert(cudaMemcpyToSymbol(c_dx, &dx, sizeof(float), 0, cudaMemcpyHostToDevice));

    float ay = 4.f / 5.f;
    float by = -1.f / 5.f;
    float cy = 4.f / 105.f;
    float dy = -1.f / 280.f;
    cuda_assert(cudaMemcpyToSymbol(c_ay, &ay, sizeof(float), 0, cudaMemcpyHostToDevice));
    cuda_assert(cudaMemcpyToSymbol(c_by, &by, sizeof(float), 0, cudaMemcpyHostToDevice));
    cuda_assert(cudaMemcpyToSymbol(c_cy, &cy, sizeof(float), 0, cudaMemcpyHostToDevice));
    cuda_assert(cudaMemcpyToSymbol(c_dy, &dy, sizeof(float), 0, cudaMemcpyHostToDevice));

    // Execution configurations for small and large pencil tiles
    grid[0] = dim3(m2, 1, 1);
    block[0] = dim3(m1, 1, 1);

    grid[1] = dim3(m1 , 1, 1);
    block[1] = dim3(1, m2, 1);

    switch(dimension)
    {
        case 0:
            derivative_x_real<<<grid[dimension],block[dimension], (m1+8)*sizeof(float) >>>(psi, dpsi, m1, m2);
            derivative_x_imag<<<grid[dimension],block[dimension], (m1+8)*sizeof(float) >>>(psi, dpsi, m1, m2);
            break;

        case 1:
            derivative_y_real<<<grid[dimension],block[dimension], (m2+8)*sizeof(float) >>>(psi, dpsi, m1, m2);
            derivative_y_imag<<<grid[dimension],block[dimension], (m2+8)*sizeof(float) >>>(psi, dpsi, m1, m2);
            break;
    }
}

void updateProbePositions(Specimen const& spec, Array* Dpsi, float* tempDpos1, int batchI)
{
    int pos = 0;
    float two = 2.f;
    float *dx_d, *dy_d;
    cufftComplex *psi_dy, *psi_dx,*Temp_Dpsi;

    float *dx_h = new float[spec.params->IM.batchSize];
    float *dy_h = new float[spec.params->IM.batchSize];

    cuda_assert(cudaMalloc((void**)&dx_d, spec.params->IM.batchSize * sizeof(float)));
    cuda_assert(cudaMalloc((void**)&dy_d, spec.params->IM.batchSize * sizeof(float)));
    initialValuesFloat<<<1,spec.params->IM.batchSize>>>(dx_d, spec.params->IM.batchSize, 0.f);
    initialValuesFloat<<<1,spec.params->IM.batchSize>>>(dy_d, spec.params->IM.batchSize, 0.f);
    cuda_assert ( cudaMalloc ( ( void** ) &psi_dx, ( spec.params->IM.ProbeDim * spec.params->IM.ProbeDim ) * sizeof ( cufftComplex ) ) );
    cuda_assert ( cudaMalloc ( ( void** ) &psi_dy, ( spec.params->IM.ProbeDim * spec.params->IM.ProbeDim ) * sizeof ( cufftComplex ) ) );
    cuda_assert ( cudaMalloc ( ( void** ) &Temp_Dpsi, ( spec.params->IM.ProbeDim * spec.params->IM.ProbeDim * spec.params->IM.batchSize ) * sizeof ( cufftComplex ) ) );

    //x-derivative of probe shape
    probeDerivative(spec.psi->values_d, psi_dx, spec.params, 0);
    //y-derivative of probe shape
    probeDerivative(spec.psi->values_d, psi_dy, spec.params, 1);
    //get dE/dpsi and conjugate as different sign is used as for probe update
    cuda_assert(cudaMemcpy(Temp_Dpsi, Dpsi->values_d, ( spec.params->IM.ProbeDim * spec.params->IM.ProbeDim * spec.params->IM.batchSize) * sizeof ( cufftComplex ), cudaMemcpyDeviceToDevice));
    complexConjugate <<< spec.params->CU.gSB, spec.params->CU.bS >>> ( Temp_Dpsi, spec.params->IM.ProbeDim * spec.params->IM.ProbeDim * spec.params->IM.batchSize );
    //dE/dpsi x dpsi/dx
    multiplyPosDerivatives<<< spec.params->CU.gSB, spec.params->CU.bS >>>(Temp_Dpsi, psi_dx, psi_dy, spec.params->IM.ProbeDim, spec.params->IM.ProbeDim, spec.params->IM.batchSize);
    //reduction for x-positions
    reduce<<< Dpsi->gS, Dpsi->bS, 32 * 32 * sizeof(float)>>>(dx_d, Temp_Dpsi, spec.params->IM.ProbeDim, 0);
    //reduction for y-position
    reduce<<< Dpsi->gS, Dpsi->bS, 32 * 32 * sizeof(float)>>>(dy_d, Temp_Dpsi, spec.params->IM.ProbeDim, 1);
    //scale by 2 for correct derivatives
    cublas_assert(cublasSscal(spec.params->CU.cublasHandle, spec.params->IM.batchSize, &two, dx_d, 1));
    cublas_assert(cublasSscal(spec.params->CU.cublasHandle, spec.params->IM.batchSize, &two, dy_d, 1));
    //copy to host
    cuda_assert(cudaMemcpy(dx_h, dx_d, spec.params->IM.batchSize * sizeof(float), cudaMemcpyDeviceToHost));
    cuda_assert(cudaMemcpy(dy_h, dy_d, spec.params->IM.batchSize * sizeof(float), cudaMemcpyDeviceToHost));

    for (int index = 0; index < spec.params->IM.batchSize; ++index)
    {
        //get correct position index
        pos = batchI * spec.params->IM.batchSize + index;
        //Update dx vector
        tempDpos1[pos*2] = dx_h[index];
        //Update y vector
        tempDpos1[pos*2+1] = dy_h[index];
    }

    delete dx_h;
    delete dy_h;

    cuda_assert(cudaFree(dx_d));
    cuda_assert(cudaFree(dy_d));
    cuda_assert(cudaFree(psi_dx));
    cuda_assert(cudaFree(psi_dy));
    cuda_assert(cudaFree(Temp_Dpsi));
}

