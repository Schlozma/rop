/*------------------------------------------------
	Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
	See ROP.cu for full notice
------------------------------------------------*/
#include <cstdio>

#include "cufft_assert.h"
#include "cuda_assert.hpp"
#include "cublas_assert.hpp"

#include "initialization.h"
#include "optimization.h"
#include "multislice.h"
#include "mathematics.h"
#include "rwBinary.h"

#include "globalVariables.h"
#include "coordArithmetic.h"

#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <thrust/device_vector.h>


/*
 * Command line argument parser
 */

//Check for file existance
bool fexists(char const* filename){
    std::ifstream ifile(filename);
    return (bool)ifile;
}

void checkArgs(int argc, char* argv[]){

    if(argc == 1)
    {
        if (fexists("ParamsInc.cnf")==false || fexists("IntensityIncSum.bin")==false)
        {
            printf("\n \nNo input files specified or found.\nStopping ROP.\n\n");
            exit(EXIT_FAILURE);
        }
        if (fexists("ParamsInc.cnf")==true && fexists("IntensityIncSum.bin")==true)
        {
            argv[1] = (char *)"ParamsInc.cnf";
            argv[2] = (char *)"IntensityIncSum.bin";
        }
    }
    else if(argc > 1)
    {
        if(fexists(argv[1])==false || fexists(argv[2])==false)
        {
            printf("\n \nCould not find specified input files.\nStopping ROP.\n\n");
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        printf("\n \nNo proper input arguments.\nStopping ROP.\n\n");
        exit(EXIT_FAILURE);
    }
}

/*
 * Array class declaration
 */

//Default constructor declaration
Array::Array(){}

//Constructor declaration
Array::Array(int X, int Y, int Z, int batch_, CU_t CU_):dimX_h(X), dimY_h(Y), dimZ_h(Z), batch(batch_), CU(CU_)
{
    //Allocate values of Array on device
    cuda_assert(cudaMalloc((void**)&values_d, dimX_h * dimY_h * dimZ_h * batch * sizeof(cufftComplex)));
    //Create grid and block size
    createGridAndBlockSize(); //CU
    //Initialize
    initialize();
}

//Copy Constructor declaration
Array::Array(Array const &A_){
    dimX_h = A_.dimX_h;
    dimY_h = A_.dimY_h;
    dimZ_h = A_.dimZ_h;
    batch = A_.batch;
    gS = A_.gS;
    bS = A_.bS;

    //Allocate values of Array on device
    cuda_assert(cudaMalloc((void**)&values_d, dimX_h * dimY_h * dimZ_h * batch * sizeof(cufftComplex)));
    //Create grid and block size
    createGridAndBlockSize(); //CU
    //Initialize
    initialize();
}

Array& Array::operator=(Array const& A_)
{
    Array tmp(A_);
    swap(tmp);
    return *this;
}

void Array::swap(Array& A_)
{
    std::swap(this->dimX_h, A_.dimX_h);
    std::swap(this->dimY_h, A_.dimY_h);
    std::swap(this->dimZ_h, A_.dimZ_h);
    std::swap(this->batch, A_.batch);
}

//Destructor
Array::~Array()
{
    if (values_d != nullptr){cuda_assert(cudaFree(values_d));}
}

void Array::createGridAndBlockSize()
{
    int maxThreads = sqrt(1024); //CU.maxThreads
    if (dimX_h < maxThreads){maxThreads = dimX_h;}
    else {maxThreads = ceil((int)dimX_h/ceil((float)dimX_h/maxThreads));}

    int b = min(dimX_h,maxThreads);
    int g = ceil((float)dimX_h/maxThreads);

    dim3 bST_(1, b, b);
    dim3 gST_(batch, g, g);
    bS = bST_;
    gS = gST_;
}

void Array::initialize()
{
    //initialize values of Array to 0.f
    initialValuesArray<<< gS, bS >>>(values_d, dimX_h, dimY_h, dimZ_h);
}

/*
 * Parameter class declaration
 */

//Default constructor declaration
Parameters::Parameters() {}

//Constructor declaration
Parameters::Parameters(std::string name):paramsFileName(name), params(new params_t), params_d(nullptr)
{
    cudaGetDeviceProperties(&prop, 0);
    defaultParams( params );
    readParams(paramsFileName, params);
    consitentParams ( params );
    setGridAndBlockSize ( params, prop );
    setCufftPlan ( params );
    setCublasHandle ( params );

    cudaMalloc ( ( void** ) &params_d, sizeof ( params_t ) );
    cuda_assert ( cudaMemcpy (params_d, params, sizeof ( params_t ), cudaMemcpyHostToDevice ) );
}

//Destructor declaration
Parameters::~Parameters(){}

void Parameters::readParams(std::string file, params_t* params)
{
	std::string line;
	std::stringstream ss;
	std::ifstream File(file.c_str());

	float x_pos, y_pos;

	while (std::getline(File, line) ) {

        if ( line.find("m0: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %g", & ( *params ).cst.m0 ); }

		if ( line.find("c: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %g", & ( *params ).cst.c ); }

		if ( line.find("e: ")!= std::string::npos )
        {   sscanf ( line.c_str(), "%*s %g", & ( *params ).cst.e ); }

		if ( line.find("h: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %g", & ( *params ).cst.h ); }

		if ( line.find("pi: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %g", & ( *params ).cst.pi ); }

		if ( line.find("E0: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.E0 ); }

		if ( line.find("gamma: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.gamma ); }

		if ( line.find("lambda: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.lambda ); }

		if ( line.find("sigma: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.sigma ); }

		if ( line.find("C1: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.C1_0, & ( *params ).EM.aberration.C1_1 ); }

		if ( line.find("A1: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.A1_0, & ( *params ).EM.aberration.A1_1 ); }

		if ( line.find("A2: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.A2_0, & ( *params ).EM.aberration.A2_1 ); }

		if ( line.find("B2: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.B2_0, & ( *params ).EM.aberration.B2_1 ); }

		if ( line.find("C3: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.C3_0, & ( *params ).EM.aberration.C3_1 ); }

		if ( line.find("A3: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.A3_0, & ( *params ).EM.aberration.A3_1 );	}

		if ( line.find("S3: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.S3_0, & ( *params ).EM.aberration.S3_1 ); }

		if ( line.find("A4: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.A4_0, & ( *params ).EM.aberration.A4_1 ); }

		if ( line.find("B4: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.B4_0, & ( *params ).EM.aberration.B4_1 ); }

		if ( line.find("D4: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.D4_0, & ( *params ).EM.aberration.D4_1 ); }

		if ( line.find("C5: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.C5_0, & ( *params ).EM.aberration.C5_1 ); }

		if ( line.find("A5: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.A5_0, & ( *params ).EM.aberration.A5_1 ); }

		if ( line.find("R5: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.R5_0, & ( *params ).EM.aberration.R5_1 ); }

		if ( line.find("S5: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g %g", & ( *params ).EM.aberration.S5_0, & ( *params ).EM.aberration.S5_1 ); }

		if ( line.find("defocspread: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.defocspread ); }

		if ( line.find("illangle: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.illangle ); }

		if ( line.find("condensorAngle: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.condensorAngle ); }

		if ( line.find("mtfa: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.mtfa ); }

		if ( line.find("mtfb: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.mtfb ); }

		if ( line.find("mtfc: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.mtfc ); }

		if ( line.find("mtfd: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.mtfd ); }

		if ( line.find("ObjAp: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).EM.ObjAp ); }

		if ( line.find("ProbeDim: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %i", & ( *params ).IM.ProbeDim ); }

		if ( line.find("ObjectDim: ")!= std::string::npos )
        { sscanf(line.c_str(), "%*s %i", &(*params).IM.ObjectDim); }

		if ( line.find("Slices: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %i", & ( *params ).IM.Slices ); }

                if ( line.find("equalSlice: ")!= std::string::npos )
        {    sscanf ( line.c_str(), "%*s %i", & ( *params ).IM.equalSlice ); }

		if ( line.find("PixelSize: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).IM.PixelSize ); }

		if ( line.find("SliceDistance: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).IM.SliceDistance ); }

		if ( line.find("CBEDDim: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %i", & ( *params ).IM.CBEDDim ); }

		if ( line.find("batchSize: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %i", & ( *params ).IM.batchSize ); }

		if ( line.find("ptyMode: ")!= std::string::npos )
        {
            sscanf(line.c_str(), "%*s %i", &(*params).SCN.ptyMode);
        }

		if ( line.find("ScanX: ")!= std::string::npos )
        {
            sscanf(line.c_str(), "%*s %i", &(*params).SCN.ScanX);
        }

		if ( line.find("ScanY: ")!= std::string::npos )
        {
            sscanf(line.c_str(), "%*s %i", &(*params).SCN.ScanY);
        }

		if ( line.find("ScanDistanceX: ")!= std::string::npos )
        {
            sscanf(line.c_str(), "%*s %g", &(*params).SCN.ScanDistanceX);
        }

		if ( line.find("ScanDistanceY: ")!= std::string::npos )
        {
            sscanf(line.c_str(), "%*s %g", &(*params).SCN.ScanDistanceY);
        }

		if ( line.find("mu: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.mu );	}


        if ( line.find("beta_wedge: ")!= std::string::npos )
        {       sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.beta_wedge ); }

        if ( line.find("sphere_radius: ")!= std::string::npos )
        {       sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.sphere_radius ); }

        if ( line.find("gauss_sigma: ")!= std::string::npos )
        {       sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.gauss_sigma ); }

        if ( line.find("alpha: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.alpha ); }

        if ( line.find("alphaIts: ")!= std::string::npos )
        { sscanf ( line.c_str(), "%*s %i", & ( *params ).RE.alpha_its );}

        if ( line.find("beta: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.beta ); }

        if ( line.find("betaIts: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %i", & ( *params ).RE.beta_its ); }

        if ( line.find("gamma: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.gamma ); }

        if ( line.find("gammaIts: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %i", & ( *params ).RE.gamma_its ); }

		if ( line.find("its: ")!= std::string::npos )
        {   sscanf ( line.c_str(), "%*s %i", & ( *params ).RE.its ); }

        if ( line.find("binning: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %i", & ( *params ).RE.binning ); }

		if ( line.find("ErrorFunction: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %i", & ( *params ).RE.ErrorFunction ); }

		if ( line.find("maskFlag: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %i", & ( *params ).RE.maskFlag ); }

		if ( line.find("cgprOrthTest: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.cgprOrthTest ); }

		if ( line.find("cgC1: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.cgC1 ); }

		if ( line.find("cgC2: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.cgC2 ); }

		if ( line.find("negAbsFctr: ")!= std::string::npos )
        {	sscanf ( line.c_str(), "%*s %g", & ( *params ).RE.negAbsFctr ); }

		if ((params->SCN.ptyMode == 1)) {

			if (line.find("beam_position:") != std::string::npos) {

				ss.str(std::string());
				ss.clear();
				ss << line.substr(15);
				ss >> x_pos >> y_pos;
				params->SCN.BeamPos.push_back(x_pos);
				params->SCN.BeamPos.push_back(y_pos);

			}
		}
	}

	if ((params->SCN.ptyMode == 0))
	{
		float i1, i2;
		float stepx = params->SCN.ScanDistanceX;
		float stepy = params->SCN.ScanDistanceY;
		const int dim1 = params->SCN.ScanX;
		const int dim2 = params->SCN.ScanY;

		for (int i = 0; i < (dim1*dim2); i++)
		{
			dbCoord ( i1, i2, i, dim1 );
			owCoordIp ( i1, dim1 );
			owCoordIp ( i2, dim2 );
			i1 = i1*stepx;
			i2 = i2*stepy;
			params->SCN.BeamPos.push_back(i1);
			params->SCN.BeamPos.push_back(i2);
		}
	}

    params->SCN.BeamPosRaw = thrust::raw_pointer_cast(params->SCN.BeamPos.data());
    params->SCN.positions = params->SCN.BeamPos.size();
}


void Parameters::writeConfig (char* file, params_t* params )
{
    FILE* fw;
    fw = fopen ( file, "wt" );

    fprintf ( fw, "# Parameter file. Let Comments be preceded by '#'\n" );
    fprintf ( fw, "\n# Nature's constants\n" );
    fprintf ( fw, "%s %14.8g %s\n", "m0: ", ( *params ).cst.m0, " # Electron rest mass (kg)" );
    fprintf ( fw, "%s %14.8g %s\n", "c: ", ( *params ).cst.c, " # Speed of light (m/s)" );
    fprintf ( fw, "%s %14.8g %s\n", "e: ", ( *params ).cst.e, " # Elementary charge (C)" );
    fprintf ( fw, "%s %14.8g %s\n", "h: ", ( *params ).cst.h, " # Planck's constant (Js)" );
    fprintf ( fw, "%s %14.8g %s\n", "pi: ", ( *params ).cst.pi, " # Pi (dimensionless)" );

    fprintf ( fw, "\n# Microscope parameters\n" );
    fprintf ( fw, "%s %14.8g %s\n", "E0: ", ( *params ).EM.E0, " # Acceleration voltage (V)" );
    fprintf ( fw, "%s %14.8g %s\n", "gamma: ", ( *params ).EM.gamma, " # From relativity: 1+e*E0/m0/c^2" );
    fprintf ( fw, "%s %14.8g %s\n", "lambda: ", ( *params ).EM.lambda, " # Electron wavelength (m)" );
    fprintf ( fw, "%s %14.8g %s\n", "sigma: ", ( *params ).EM.sigma, " # Interaction constant ( 1/(Vm) )" );

    fprintf ( fw, "%s %14.8g %14.8g %s\n", "C1: ", ( *params ).EM.aberration.C1_0 + params->RE.defocPoly0, ( *params ).EM.aberration.C1_1, " # Focus value (m)" );
    fprintf ( fw, "%s %14.8g %14.8g %s\n", "A1: ", ( *params ).EM.aberration.A1_0, ( *params ).EM.aberration.A1_1, " # Astigmatism (m) and (rad)" );
    fprintf ( fw, "%s %14.8g %14.8g\n", "A2: ", ( *params ).EM.aberration.A2_0, ( *params ).EM.aberration.A2_1 );
    fprintf ( fw, "%s %14.8g %14.8g\n", "B2: ", ( *params ).EM.aberration.B2_0, ( *params ).EM.aberration.B2_1 );
    fprintf ( fw, "%s %14.8g %14.8g %s\n", "C3: ", ( *params ).EM.aberration.C3_0, ( *params ).EM.aberration.C3_1, " # Spherical aberration (m)" );
    fprintf ( fw, "%s %14.8g %14.8g\n", "A3: ", ( *params ).EM.aberration.A3_0, ( *params ).EM.aberration.A3_1 );
    fprintf ( fw, "%s %14.8g %14.8g\n", "S3: ", ( *params ).EM.aberration.S3_0, ( *params ).EM.aberration.S3_1 );
    fprintf ( fw, "%s %14.8g %14.8g\n", "A4: ", ( *params ).EM.aberration.A4_0, ( *params ).EM.aberration.A4_1 );
    fprintf ( fw, "%s %14.8g %14.8g\n", "B4: ", ( *params ).EM.aberration.B4_0, ( *params ).EM.aberration.B4_1 );
    fprintf ( fw, "%s %14.8g %14.8g\n", "D4: ", ( *params ).EM.aberration.D4_0, ( *params ).EM.aberration.D4_1 );
    fprintf ( fw, "%s %14.8g %14.8g\n", "C5: ", ( *params ).EM.aberration.C5_0, ( *params ).EM.aberration.C5_1 );
    fprintf ( fw, "%s %14.8g %14.8g\n", "A5: ", ( *params ).EM.aberration.A5_0, ( *params ).EM.aberration.A5_1 );
    fprintf ( fw, "%s %14.8g %14.8g\n", "R5: ", ( *params ).EM.aberration.R5_0, ( *params ).EM.aberration.R5_1 );
    fprintf ( fw, "%s %14.8g %14.8g\n", "S5: ", ( *params ).EM.aberration.S5_0, ( *params ).EM.aberration.S5_1 );

    fprintf ( fw, "%s %14.8g %s\n", "defocspread: ", ( *params ).EM.defocspread, " # Defocus spread for the temporal partial coherence (m)" );
    fprintf ( fw, "%s %14.8g %s\n", "illangle: ", ( *params ).EM.illangle, " # illumination half angle characterizing the spatial partial coherence (rad)" );
    fprintf ( fw, "%s %14.8g %s\n", "condensorAngle: ", ( *params ).EM.condensorAngle, " # Half-angle of the illumination (rad)" );
    fprintf ( fw, "%s %14.8g %s\n", "ObjAp: ", ( *params ).EM.ObjAp, " # DIAMETER of the objective aperture (rad)" );

    fprintf ( fw, "\n# Imaging parameters\n" );
    fprintf(fw, "%s %i %s\n", "ObjectDim: ", (*params).IM.ObjectDim, " # Length of 1st real dimension of the object");
    fprintf ( fw, "%s %i %s\n", "ProbeDim: ", ( *params ).IM.ProbeDim, " # Length of 1st dimension of the object" );
    fprintf ( fw, "%s %i %s\n", "number of slices: ", ( *params ).IM.Slices, " # Number of slices of the object" );
    fprintf ( fw, "%s %14.8g %s\n", "pixel size: ", ( *params ).IM.PixelSize, " # Length of 1st dimension of the voxels (m)" );
    fprintf ( fw, "%s %14.8g %s\n", "slice distance: ", ( *params ).IM.SliceDistance, " # Length of 3rd dimension of the voxels (m)" );
    fprintf ( fw, "%s %i %s\n", "dn: ", ( *params ).IM.dn, " # Padding, m1 = n1 + 2*dn1" );
    fprintf ( fw, "%s %i %s\n", "CBEDDim: ", ( *params ).IM.CBEDDim, " # Length of 1st dimension of the measurements" );

    fprintf ( fw, "\n# Reconstruction parameters\n" );
    fprintf ( fw, "%s %14.8g %s\n", "mu: ", ( *params ).RE.mu, " # Weights of the L1-norm (dimensionless)" );
    fprintf ( fw, "%s %i %s\n", "its: ", ( *params ).RE.its, " # Number of iterations" );
    fprintf ( fw, "%s %i %s\n", "ErrorFunction: ", ( *params ).RE.ErrorFunction, " # Kind of error function. 0: Sum of squared differences, 1: Sum of abs. val. of the difference, 2: Sum of abs. val. of differences divided by sqrt(V)" );
    fprintf ( fw, "%s %i %s\n", "maskFlag: ", ( *params ).RE.maskFlag, " # Filter to use on the raw reconstruction. 0 none, 1 2DRingfilter" );
    fprintf ( fw, "%s %14.8g %s\n", "cgprOrthTest: ", ( *params ).RE.cgprOrthTest, " # Threshold value for the test if to consecutive gradients in CG-PR are orthogonal enough" );
    fprintf ( fw, "%s %14.8g %s\n", "cgC1: ", ( *params ).RE.cgC1, " # Parameter of the strong Wolfe conditions for sufficient descent of the function value" );
    fprintf ( fw, "%s %14.8g %s\n", "cgC2: ", ( *params ).RE.cgC2, " # Parameter of the strong Wolfe conditions for sufficient descent of the function derivative" );
    fprintf ( fw, "%s %14.8g %s\n", "negAbsFctr: ", ( *params ).RE.negAbsFctr, " # Factor with which to multiply the absolute values of negative numbers in the solution" );
    fprintf ( fw, "%s %i %s\n", "noNuisParams: ", ( *params ).RE.noNuisParams, " # Number of nuissance params to estimate" );


    fprintf ( fw, "\n# Grid and block sizes\n" );
    fprintf ( fw, "%s %i\n", "gS: ",   ( *params ).CU.gS );
    fprintf ( fw, "%s %i\n", "gS2D: ", ( *params ).CU.gS2D );
    fprintf ( fw, "%s %i\n", "bS: ",   ( *params ).CU.bS );

    fclose ( fw );
}


void Parameters::defaultParams( params_t* params )
{
	params->cst.m0 = 9.1093822e-31f;
    params->cst.c  = 2.9979246e8f;
    params->cst.e  = 1.6021766e-19f;
    params->cst.h  = 6.6260696e-34f;
    params->cst.pi = 3.1415927f;

    params->EM.E0 = 200e3;
	params->EM.gamma = 1.3913902f;
    params->EM.lambda = 2.507934e-012f;
	params->EM.sigma = 7288400.5f;

    params->EM.aberration.C1_0 = -6.1334e-008f;
    params->EM.aberration.A1_0 = 0.f;
    params->EM.aberration.A2_0 = 0.f;
    params->EM.aberration.B2_0 = 0.f;
    params->EM.aberration.C3_0 = 1e-3f;
    params->EM.aberration.A3_0 = 0.f;
    params->EM.aberration.S3_0 = 0.f;
    params->EM.aberration.A4_0 = 0.f;
    params->EM.aberration.B4_0 = 0.f;
    params->EM.aberration.D4_0 = 0.f;
    params->EM.aberration.C5_0 = 0.f;
    params->EM.aberration.A5_0 = 0.f;
    params->EM.aberration.R5_0 = 0.f;
    params->EM.aberration.S5_0 = 0.f;

    params->EM.aberration.C1_1 = 0.f;
    params->EM.aberration.A1_1 = 0.f;
    params->EM.aberration.A2_1 = 0.f;
    params->EM.aberration.B2_1 = 0.f;
    params->EM.aberration.C3_1 = 0.f;
    params->EM.aberration.A3_1 = 0.f;
    params->EM.aberration.S3_1 = 0.f;
    params->EM.aberration.A4_1 = 0.f;
    params->EM.aberration.B4_1 = 0.f;
    params->EM.aberration.D4_1 = 0.f;
    params->EM.aberration.C5_1 = 0.f;
    params->EM.aberration.A5_1 = 0.f;
    params->EM.aberration.R5_1 = 0.f;
    params->EM.aberration.S5_1 = 0.f;

    params->EM.defocspread = 0.f;
    params->EM.illangle = 0.f;
    params->EM.condensorAngle = 0.f;
    params->EM.mtfa = 1.f;
    params->EM.mtfb = 0.f;
    params->EM.mtfc = 0.f;
    params->EM.mtfd = 0.f;
    params->EM.ObjAp = 1.5707963f;

    params->IM.ObjectDim = 4;
    params->IM.ProbeDim = 4;
    params->IM.Slices = 1;
    params->IM.equalSlice = 0;
    params->IM.PixelSize = 0.25e-10f;
    params->IM.SliceDistance = 2e-10f;
    params->IM.dn = 1;
    params->IM.CBEDDim = 2;
    params->IM.batchSize = 1;

    params->SCN.ptyMode = 0;
    params->SCN.ScanX = 1;
    params->SCN.ScanY = 1;
    params->SCN.ScanDistanceX = 0.25e-10f;
    params->SCN.ScanDistanceY = 0.25e-10f;
    params->SCN.positions = 1;

    params->RE.mu = 1.f;
    params->RE.beta_wedge = 1.f;
    params->RE.sphere_radius = 0.f;
    params->RE.gauss_sigma = 0.f;
    params->RE.alpha = 1e-3f;
    params->RE.alpha_its = 0;
    params->RE.beta = 1e-3f;
    params->RE.beta_its = 0;
    params->RE.gamma = 1e-3f;
    params->RE.gamma_its = 0;
    params->RE.its = 100;
    params->RE.binning = 0;
    params->RE.maskFlag = 0;
    params->RE.ErrorFunction = 0;

    params->RE.noNuisParams = 1;
    params->RE.cgprOrthTest = 0.5f;
    params->RE.cgC1 = 1e-4f;
    params->RE.cgC2 = 0.1f;
    params->RE.negAbsFctr = 1.f;

    params->RE.defocPoly0 = 0.f;
    params->RE.defocPoly1 = 1;
    params->RE.defocPoly2 = 0.f;
}


void Parameters::consitentParams ( params_t* params )
{
    const float E0 = params->EM.E0;
    const float m0 = 9.1093822f;
    const float c  = 2.9979246f;
    const float e  = 1.6021766f;
    const float h  = 6.6260696f;
    const float pi = params->cst.pi;

    params->EM.gamma = 1.f + E0 * e / m0 / c / c * 1e-4f;
    params->EM.lambda = h / sqrt ( 2.f * m0 * e ) * 1e-9f / sqrt ( E0 * ( 1.f + E0 * e / 2.f / m0 / c / c * 1e-4f ) ); // electron wavelength (m), see De Graef p. 92
    params->EM.sigma =  2.f * pi * params->EM.gamma * params->EM.lambda * m0 * e / h / h * 1e18f; // interaction constant (1/(Vm))
    params->EM.condensorAngle = fabsf( params->EM.condensorAngle );

    params->IM.dn = ceil( (params->IM.ProbeDim - params->IM.CBEDDim) / 2);
    params->IM.ProbeDim = params->IM.CBEDDim + 2 * params->IM.dn;

    if ( params->RE.negAbsFctr < 0.f )
    {    params->RE.negAbsFctr = 0.f; }

    if( params->EM.aberration.A1_0 < 0.f )
    {
    	params->EM.aberration.A1_0 = fabsf( params->EM.aberration.A1_0 );
	params->EM.aberration.A1_1 += params->cst.pi;
    }
    if( params->EM.aberration.A2_0 < 0.f )
    {
   	params->EM.aberration.A2_0 = fabsf( params->EM.aberration.A2_0 );
  	params->EM.aberration.A2_1 += params->cst.pi;
    }
    if( params->EM.aberration.B2_0 < 0.f )
    {
  	params->EM.aberration.B2_0 = fabsf( params->EM.aberration.B2_0 );
 	params->EM.aberration.B2_1 += params->cst.pi;
     }
}

void Parameters::copyParams ( params_t* dst, params_t* src )
{
    dst->cst.m0 = src->cst.m0;
    dst->cst.c  = src->cst.c;
    dst->cst.e  = src->cst.e;
    dst->cst.h  = src->cst.h;
    dst->cst.pi  = src->cst.pi;

    dst->EM.E0 = src->EM.E0;
    dst->EM.gamma = src->EM.gamma;
    dst->EM.lambda = src->EM.lambda;
    dst->EM.sigma = src->EM.sigma;

    dst->EM.aberration.C1_0 = src->EM.aberration.C1_0;
    dst->EM.aberration.A1_0 = src->EM.aberration.A1_0;
    dst->EM.aberration.A2_0 = src->EM.aberration.A2_0;
    dst->EM.aberration.B2_0 = src->EM.aberration.B2_0;
    dst->EM.aberration.C3_0 = src->EM.aberration.C3_0;
    dst->EM.aberration.A3_0 = src->EM.aberration.A3_0;
    dst->EM.aberration.S3_0 = src->EM.aberration.S3_0;
    dst->EM.aberration.A4_0 = src->EM.aberration.A4_0;
    dst->EM.aberration.B4_0 = src->EM.aberration.B4_0;
    dst->EM.aberration.D4_0 = src->EM.aberration.D4_0;
    dst->EM.aberration.C5_0 = src->EM.aberration.C5_0;
    dst->EM.aberration.A5_0 = src->EM.aberration.A5_0;
    dst->EM.aberration.R5_0 = src->EM.aberration.R5_0;
    dst->EM.aberration.S5_0 = src->EM.aberration.S5_0;

    dst->EM.aberration.C1_1 = src->EM.aberration.C1_1;
    dst->EM.aberration.A1_1 = src->EM.aberration.A1_1;
    dst->EM.aberration.A2_1 = src->EM.aberration.A2_1;
    dst->EM.aberration.B2_1 = src->EM.aberration.B2_1;
    dst->EM.aberration.C3_1 = src->EM.aberration.C3_1;
    dst->EM.aberration.A3_1 = src->EM.aberration.A3_1;
    dst->EM.aberration.S3_1 = src->EM.aberration.S3_1;
    dst->EM.aberration.A4_1 = src->EM.aberration.A4_1;
    dst->EM.aberration.B4_1 = src->EM.aberration.B4_1;
    dst->EM.aberration.D4_1 = src->EM.aberration.D4_1;
    dst->EM.aberration.C5_1 = src->EM.aberration.C5_1;
    dst->EM.aberration.A5_1 = src->EM.aberration.A5_1;
    dst->EM.aberration.R5_1 = src->EM.aberration.R5_1;
    dst->EM.aberration.S5_1 = src->EM.aberration.S5_1;

    dst->EM.defocspread = src->EM.defocspread;
    dst->EM.illangle = src->EM.illangle;
    dst->EM.condensorAngle = src->EM.condensorAngle;
    dst->EM.mtfa = src->EM.mtfa;
    dst->EM.mtfb = src->EM.mtfb;
    dst->EM.mtfc = src->EM.mtfc;
    dst->EM.mtfd = src->EM.mtfd;
    dst->EM.ObjAp = src->EM.ObjAp;

    dst->IM.ObjectDim = src->IM.ObjectDim;
    dst->IM.ProbeDim = src->IM.ProbeDim;
    dst->IM.Slices = src->IM.Slices;
    dst->IM.PixelSize = src->IM.PixelSize;
    dst->IM.SliceDistance = src->IM.SliceDistance;
    dst->IM.CBEDDim = src->IM.CBEDDim;
    dst->IM.batchSize = src->IM.batchSize;

    dst->SCN.ptyMode = src->SCN.ptyMode;
    dst->SCN.ScanX = src->SCN.ScanX;
    dst->SCN.ScanY = src->SCN.ScanY;
    dst->SCN.ScanDistanceX = src->SCN.ScanDistanceX;
    dst->SCN.ScanDistanceY = src->SCN.ScanDistanceY;
    dst->SCN.positions = src->SCN.positions;

    dst->SCN.BeamPos = src->SCN.BeamPos;
    dst->SCN.BeamPosRaw = thrust::raw_pointer_cast(src->SCN.BeamPos.data());

    dst->RE.mu = src->RE.mu;
    dst->RE.beta_wedge = src->RE.beta_wedge;
    dst->RE.sphere_radius = src->RE.sphere_radius;
    dst->RE.gauss_sigma = src->RE.gauss_sigma;
    dst->RE.alpha = src->RE.alpha;
    dst->RE.alpha_its = src->RE.alpha_its;
    dst->RE.beta = src->RE.beta;
    dst->RE.beta_its = src->RE.beta_its;
    dst->RE.gamma = src->RE.gamma;
    dst->RE.gamma_its = src->RE.gamma_its;
    dst->RE.its = src->RE.its;
    dst->RE.binning = src->RE.binning;
    dst->RE.maskFlag = src->RE.maskFlag;
    dst->RE.ErrorFunction = src->RE.ErrorFunction;

    dst->RE.noNuisParams = src->RE.noNuisParams;
    dst->RE.cgprOrthTest = src->RE.cgprOrthTest;
    dst->RE.cgC1 = src->RE.cgC1;
    dst->RE.cgC2 = src->RE.cgC2;
    dst->RE.negAbsFctr = src->RE.negAbsFctr;

    dst->RE.defocPoly0 = src->RE.defocPoly0;
    dst->RE.defocPoly1 = src->RE.defocPoly1;
    dst->RE.defocPoly2 = src->RE.defocPoly2;
}

void Parameters::setCufftPlan ( params_t* params )
{
    cufft_assert ( cufftPlan2d ( & ( params->CU.cufftPlan ), params->IM.ProbeDim, params->IM.ProbeDim, CUFFT_C2C ) );
    cufft_assert ( cufftPlan2d ( & ( params->CU.cufftPlan_l ), params->IM.ObjectDim, params->IM.ObjectDim, CUFFT_C2C ) );
    cufft_assert ( cufftPlan3d ( & ( params->CU.cufftPlan3D ), params->IM.Slices, params->IM.ObjectDim, params->IM.ObjectDim, CUFFT_C2C ) );

    int embed[2]={params->IM.ProbeDim, params->IM.ProbeDim};
    cufft_assert( cufftPlanMany(& params->CU.cufftPlanMany, 2, embed, NULL, 1, 0, NULL, 1, 0, CUFFT_C2C, params->IM.batchSize));
}

void Parameters::setCublasHandle ( params_t* params )
{
    cublas_assert ( cublasCreate ( & ( params->CU.cublasHandle ) ) );
}

void Parameters::setGridAndBlockSize ( params_t* params, cudaDeviceProp prop )
{
    const int maxBS = prop.maxThreadsDim[0]; // Maximum blocksize allowed by device.
    const int ProbeDim = params->IM.ProbeDim;
    const int batchSize = params->IM.batchSize;

    params->CU.bS = maxBS;
    params->CU.gS = ( params->IM.ProbeDim * params->IM.ProbeDim * ( params->IM.Slices + 3 ) ) / params->CU.bS + 1;

    int ProbeDim2D = params->IM.ProbeDim * params->IM.ProbeDim;
    params->CU.gS2D = (ProbeDim2D % maxBS == 0) ? (ProbeDim2D / maxBS) : (ProbeDim2D / maxBS + 1);

    int ObjectDim2D = params->IM.ObjectDim * params->IM.ObjectDim;
    params->CU.gS2D_l = (ObjectDim2D % maxBS == 0) ? (ObjectDim2D / maxBS) : (ObjectDim2D / maxBS + 1);

    int ProbeDim3DB = params->IM.ProbeDim * params->IM.ProbeDim * params->IM.Slices * params->IM.batchSize;
    params->CU.gSB = (ProbeDim3DB % maxBS == 0) ? (ProbeDim3DB / maxBS) : (ProbeDim3DB / maxBS + 1);

    // Create grid and block size for Probe dimension
    int maxThreads = sqrt(maxBS);
    if (ProbeDim < maxThreads){maxThreads = ProbeDim;}
    else {maxThreads = ceil((int)ProbeDim/ceil((float)ProbeDim/maxThreads));}

    int b = min(ProbeDim,maxThreads);
    int g = ceil((float)ProbeDim/maxThreads);

    dim3 bST_(1,b,b);
    dim3 gST_(batchSize,g,g);
    params->CU.bST = bST_;
    params->CU.gST = gST_;
}

/*
 * Specimen class declaration
 */

//Default Constructor declaration
Specimen::Specimen(){}

//Constructor declaration
Specimen::Specimen(int argc, char* argv[]):Parameters(argv[1])
{
    //Allocate memory for residual and regularization
    E_h = ( float* ) calloc ( params->RE.its, sizeof ( float ) );
    L1_h = ( float* ) calloc ( params->RE.its, sizeof ( float ) );
    //Allocate memory for object
    V = new Array(params->IM.ObjectDim, params->IM.ObjectDim, params->IM.Slices, 1, params->CU);
    if(argc > 4)
    {readInitialPotential (V->values_d, argv, params);}
    //Allocate memory for object derivative
    dE0 = new Array(params->IM.ObjectDim, params->IM.ObjectDim, params->IM.Slices, 1, params->CU);
    dE1 = new Array(params->IM.ObjectDim, params->IM.ObjectDim, params->IM.Slices, 1, params->CU);
    D = new Array(params->IM.ObjectDim, params->IM.ObjectDim, params->IM.Slices, 1, params->CU);
    //Allocate memory for probe
    psi = new Array(params->IM.ProbeDim, params->IM.ProbeDim, (3 + params->IM.Slices), params->IM.batchSize, params->CU);
    //initialize probe
    initialProbe(psi, argc, argv, params, params_d);
    //Allocate memory for probe derivative
    dpsi0 = new Array(params->IM.ProbeDim, params->IM.ProbeDim, 1, 1, params->CU);
    dpsi1 = new Array(params->IM.ProbeDim, params->IM.ProbeDim, 1, 1, params->CU);
    Dpsi = new Array(params->IM.ProbeDim, params->IM.ProbeDim, 1, 1, params->CU);
    //Allocate memory for positions
    cuda_assert(cudaMalloc((void**)&dpos0, params->SCN.positions * sizeof(float)));
    cuda_assert(cudaMalloc((void**)&dpos1, params->SCN.positions * sizeof(float)));
    cuda_assert(cudaMalloc((void**)&Dpos, params->SCN.positions * sizeof(float)));
    //initialize positions
    initialValuesFloat<<< params->SCN.positions + 1, (params->SCN.positions/1024)>>>(dpos0, params->SCN.positions, 0.f);
    initialValuesFloat<<< params->SCN.positions + 1, (params->SCN.positions/1024)>>>(dpos1, params->SCN.positions, 0.f);
    initialValuesFloat<<< params->SCN.positions + 1, (params->SCN.positions/1024)>>>(Dpos, params->SCN.positions, 0.f);
    //Allocate memory on host and device for input data
    I_h = new float[params->IM.CBEDDim*params->IM.CBEDDim* params->SCN.BeamPos.size()/2];
    cuda_assert ( cudaMalloc ( ( void** ) &I_d, params->IM.CBEDDim*params->IM.CBEDDim* params->SCN.BeamPos.size()/2 * sizeof(float)));
    //Read input data and store on host and device
    readBinary (argv[2], I_h, params->IM.CBEDDim*params->IM.CBEDDim* params->SCN.BeamPos.size()/2); //DataFile.c_str()
    cuda_assert ( cudaMemcpy (I_d, I_h, params->IM.CBEDDim*params->IM.CBEDDim* params->SCN.BeamPos.size()/2 * sizeof ( float ), cudaMemcpyHostToDevice ) );
}

//Destructor declaration
Specimen::~Specimen()
{
    free ( E_h );
    free ( L1_h );
    delete V;
    delete dE0;
    delete dE1;
    delete D;
    delete psi;
    delete dpsi0;
    delete dpsi1;
    delete Dpsi;
    cuda_assert(cudaFree(dpos0));
    cuda_assert(cudaFree(dpos1));
    cuda_assert(cudaFree(Dpos));

    if (I_h != nullptr){delete[] I_h;}
    if (I_d != nullptr){cuda_assert(cudaFree(I_d));}
}

void Specimen::initialProbe(Array* psi, int argc, char* argv[], params_t* params, params_t* params_d)
{
    //Construct incoming wave
    cufftComplex* temp;
    cuda_assert ( cudaMalloc ( ( void** ) &temp, (psi->dimX_h * psi->dimY_h) * sizeof ( cufftComplex ) ) );
    incomingWave(temp, params, params_d );
    if(argc > 3)
    {readInitialPsi(temp, argv, params);}
    copyIn<<< psi->gS, psi->bS >>>(psi->values_d, temp, params_d, 0);
    cuda_assert(cudaFree(temp));
}

void Specimen::copyProbe(Array* psi, Array* psi_, params_t* params, params_t* params_d, params_t* params_D)
{
    cufftComplex* temp;
    cuda_assert ( cudaMalloc ( ( void** ) &temp, (psi->dimX_h * psi->dimY_h) * sizeof ( cufftComplex ) ) );
    cropIn<<< psi->gS, psi->bS >>>(temp, psi_->values_d, params_d, params_D);
    copyIn<<< psi->gS, psi->bS >>>(psi->values_d, temp, params_d, 0);
    cuda_assert(cudaFree(temp));
}

void readInitialPsi ( cufftComplex* psi, char* argv[], params_t* params )
{
    float* psiri;
    cufftComplex* psi_h;
    const int size = params->IM.ProbeDim * params->IM.ProbeDim;
    psiri = ( float* ) malloc ( size * sizeof ( float ) );
    psi_h = ( cufftComplex* ) malloc ( size * sizeof ( cufftComplex ) );

    char name[128];
    *name = 0;

    strcpy(name,argv[3]);
    strcat(name,"_re.bin");

    readBinary (name, psiri, size );

    for ( int j = 0; j < size; j++ )
    { psi_h[j].x = psiri[j]; }

    strcpy(name,argv[3]);
    strcat(name,"_im.bin");

    readBinary (name, psiri, size );

    for ( int j = 0; j < size; j++ )
    { psi_h[j].y = psiri[j]; }

    cuda_assert ( cudaMemcpy ( psi, psi_h, size * sizeof ( cufftComplex ), cudaMemcpyHostToDevice ) );

    free ( psiri );
    free ( psi_h );
}

void readInitialPotential ( cufftComplex* V, char* argv[], params_t* params )
{
    float* Vri;
    cufftComplex* V_h;
    const int size = params->IM.ObjectDim * params->IM.ObjectDim;
    Vri = ( float* ) malloc ( size * sizeof ( float ) );
    V_h = ( cufftComplex* ) malloc ( size * sizeof ( cufftComplex ) );

    char name[128];
    *name = 0;

    strcpy(name,argv[4]);
    strcat(name,"_re.bin");

    readBinary (name, Vri, size );

    for ( int j = 0; j < size; j++ )
    { V_h[j].x = Vri[j]; }

    strcpy(name,argv[4]);
    strcat(name,"_im.bin");

    readBinary (name, Vri, size );

    for ( int j = 0; j < size; j++ )
    { V_h[j].y = Vri[j]; }

    cuda_assert ( cudaMemcpy ( V, V_h, size * sizeof ( cufftComplex ), cudaMemcpyHostToDevice ) );

    free ( Vri );
    free ( V_h );
}

