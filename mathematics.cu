/*------------------------------------------------
	Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
	See ROP.cu for full notice
------------------------------------------------*/

#include "mathematics.h"
#include "coordArithmetic.h"
#include "initialization.h"

__global__ void reduce(float* fOut, cufftComplex* fIn, int size, int flag)
{
    extern __shared__ float sdata[];

    //Set indices for input data and shared data
    int tid = threadIdx.y + 32 * threadIdx.z;
    int id = threadIdx.y + blockDim.y * blockIdx.y + size * (threadIdx.z + blockDim.z * blockIdx.z) + size * size * blockIdx.x;

    if(flag == 0)
    {
        if ((threadIdx.y + blockDim.y * blockIdx.y < size) &&
            (threadIdx.z + blockDim.z * blockIdx.z < size)) { sdata[tid] = fIn[id].x; }
        else { sdata[tid] = 0.f; }
        __syncthreads();
    }

    if(flag == 1)
    {
        if ((threadIdx.y + blockDim.y * blockIdx.y < size) &&
            (threadIdx.z + blockDim.z * blockIdx.z < size)) { sdata[tid] = fIn[id].y; }
        else { sdata[tid] = 0.f; }
        __syncthreads();
    }

    if(threadIdx.y < (32 - blockDim.y))
    {
        sdata[tid + blockDim.y] = 0.f;
    }

    if(threadIdx.z < (32 - blockDim.z))
    {
        sdata[tid + 32 * blockDim.z] = 0.f;
    }

    for( unsigned int s=1; s < (32 * 32); s *=2 )
    {
        if(tid%(2*s)==0){sdata[tid] += sdata[tid + s];}
        __syncthreads();
    }

    if(tid == 0){atomicAdd(&(fOut[blockIdx.x]), sdata[0]);}
}

__global__ void complexConjugate(cufftComplex* src, int size)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if(i < size)
    {
        src[i].x = src[i].x;
        src[i].y = -src[i].y;
    }

}

void realPart(float* Vri, cufftComplex* V, int size)
{
        for (int i = 0; i < size; i++)
        {
                Vri[i] = V[i].x;
        }
}

void imagPart(float* Vri, cufftComplex* V, int size)
{
        for (int i = 0; i < size; i++)
        {
                Vri[i] = V[i].y;
        }
}


__global__ void multiplyElementwise(cufftComplex* f0, cufftComplex* f1, params_t* params, int flag)
{
	const int i = blockIdx.x * blockDim.x + threadIdx.x;
	const int m1 = params->IM.ProbeDim;
        const int m2 = params->IM.ProbeDim;
	const int batchSize = params->IM.batchSize;
	int s, t, i1, i2, i3;
	int size = m1 * m2 * batchSize;

	trCoord(i1,i2,i3,i,m1,m2);
        sgCoord3D(s,i1,i2,i3,m1,m2);

        if(flag==0)
        { sgCoord(t, i1, i2, m1);}
	if(flag==1)
	{ t = s; }

	if (i < size)
	{
		float a, b, c, d;
		a = f0[s].x;
		b = f0[s].y;
		c = f1[t].x;
		d = f1[t].y;
		float k;
		k = a * (c + d);
		d *= a + b;
		c *= b - a;
		f0[s].x = k - d;
		f0[s].y = k + c;
	}
}

__global__ void multiplyPosDerivatives(cufftComplex* f0, cufftComplex* fx, cufftComplex* fy, int m1, int m2, int batchSize)
{

    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    int s,t,i1,i2,i3;
    int size = m1 * m2 * batchSize;

    trCoord(i1,i2,i3,i,m1,m2);
    sgCoord3D(s,i1,i2,i3,m1,m2);
    sgCoord(t,i1,i2,m1);

    if (i < size)
    {
        float a, b, c, d, e, f;
        a = f0[s].x;
        b = f0[s].y;
        c = fx[t].x;
        d = fx[t].y;
        e = fy[t].x;
        f = fy[t].y;
        f0[s].x = a * c - b * d;
        f0[s].y = a * e - b * f;
    }
}

__global__ void divideBy ( float* f1, float* f2, int size )
{
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    const float eps = 7.377e-6f;

    if ( i < size )
        {    f1[i] /= ( f2[i] + eps); }

}

__global__ void addEpsilon ( float* f, int size )
{
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
     const float eps = 7.377e-6f;

    if ( i < size )
        {    f[i] += eps ; }

}

__global__ void logValues ( float* f1, float* f2, int size )
{
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    const float eps = 7.377e-6f;

    if ( i < size )
        {    f1[i] *= logf( f2[i] + eps); }

}

__global__ void signValues ( float* f, int size )
{
        const int i = blockIdx.x * blockDim.x + threadIdx.x;
        float eps = 1e-9f;

    if ( i < size )
        {
                if ( f[i] < -eps )
                {    f[i] = -1.f; }
                else { if ( f[i] > eps )
                {    f[i] = 1.f; }
                else
                {    f[i] = 0.f; } }
        }
}

__global__ void sumValues(cufftComplex* temp, cufftComplex* V, params_t* params)
{
    int i1 = threadIdx.z + blockDim.z * blockIdx.z;
    int i2 = threadIdx.y + blockDim.y * blockIdx.y;

    int index_t, index_v;
    const int l1 = params->IM.ObjectDim;
    const int l2 = params->IM.ObjectDim;
    const int slices = params->IM.Slices;

    sgCoord3D(index_t, i1, i2, 0, l1, l2);

    for (int slice = 0; slice < slices; slice += 1)
    {
        sgCoord3D(index_v, i1, i2, slice, l1, l2);
        if ((i1 > -1) && (i1 < l1) && (i2 > -1) && (i2 < l2))
        {
            atomicAdd(&(temp[index_t].x), V[index_v].x);
            atomicAdd(&(temp[index_t].y), V[index_v].y);
        }
    }

    __syncthreads();

    for (int slice = 0; slice < slices; slice += 1)
    {
        sgCoord3D(index_v, i1, i2, slice, l1, l2);
        if ((i1 > -1) && (i1 < l1) && (i2 > -1) && (i2 < l2))
        {
            V[index_v].x = (temp[index_t].x / slices);
	    V[index_v].y = (temp[index_t].y / slices);
        }
    }
}

__global__ void copySingle2Double ( double* fD, float* fS,  int size )
{
        const int i = blockIdx.x * blockDim.x + threadIdx.x;

    if ( i < size )
        {    fD[i] = (double) fS[i];    }
}


__global__ void copyIn ( cuComplex* psi, cufftComplex* temp, params_t* params, int j )
{

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;

    const int m1 = params->IM.ProbeDim;
    const int m2 = params->IM.ProbeDim;
    const int Slices = params->IM.Slices;

    int s,t;
    sgCoord3D(s,i1,i2, (Slices + 3) * blockIdx.x,m1,m2);
    sgCoord(t,i1,i2,m1);

    psi[s].x = temp[t].x;
    psi[s].y = temp[t].y;

}

__global__ void cropIn ( cuComplex* psi, cuComplex* psi_, params_t* params, params_t* params_)
{

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;

    const int m1 = params->IM.ProbeDim;
    const int m1_ = params_->IM.ProbeDim;
    const int sd = (int)(m1_ - m1)/2;

    int s,t;
    sgCoord(s, i1 + sd, i2 + sd, m1_);
    sgCoord(t, i1, i2, m1);

    psi[t].x = psi_[s].x;
    psi[t].y = psi_[s].y;

}

__global__ void addIn ( cuComplex* psi, cufftComplex* temp, params_t* params, int j )
{
    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;

    const int m1 = params->IM.ProbeDim;
    const int m2 = params->IM.ProbeDim;
    const int Slices = params->IM.Slices;

    int s,t;
    sgCoord3D(s,i1,i2, (Slices + 3) * blockIdx.x,m1,m2);
    sgCoord(t,i1,i2,m1);

    if ((i1 > -1) && (i1 < m1) && (i2 > -1) && (i2 < m2 ))
    {
        atomicAdd(&(psi[s].x), temp[t].x);
        atomicAdd(&(psi[s].y), temp[t].y);
    }
}

__global__ void copyPsiBatchInOut ( cufftComplex* f0, cufftComplex* f1, params_t* params, int j, int flag )
{
    const int m1 = params->IM.ProbeDim;
    const int m2 = params->IM.ProbeDim;
    const int Slices = params->IM.Slices;

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;
    int i3 = ((Slices + 3) * blockIdx.x) + j;

    int s,t;
    sgCoord3D(s, i1, i2, i3 , m1, m2);
    sgCoord3D(t, i1, i2, blockIdx.x, m1, m2);

    if ((i1 > -1) && (i1 < m1 ) && (i2 > -1) && (i2 < m2 ))
    {
        if(flag==1)
        {
            f0[t].x = f1[s].x;
            f0[t].y = f1[s].y;
        }

        if(flag==0)
        {
            f0[s].x = f1[s].x;
            f0[s].y = f1[s].y;
        }

        if(flag==-1)
        {
            f0[s].x = f1[t].x;
            f0[s].y = f1[t].y;
        }
    }
}

__global__ void copyBatchInOut ( cufftComplex* f0, cufftComplex* f1, params_t* params, int j, int flag )
{
    const int m1 = params->IM.ProbeDim;
    const int m2 = params->IM.ProbeDim;
    const int Slices = params->IM.Slices;

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;
    int i3 = (Slices * blockIdx.x) + j;

    int s,t;
    sgCoord3D(s,i1,i2, i3 ,m1,m2);
    sgCoord3D(t,i1,i2,blockIdx.x, m1,m2);

    if ((i1 > -1) && (i1 < m1 ) && (i2 > -1) && (i2 < m2 ))
    {
        if(flag==1)
        {
            f0[t].x = f1[s].x;
            f0[t].y = f1[s].y;
        }

        if(flag==-1)
        {
            f0[s].x = f1[t].x;
            f0[s].y = f1[t].y;
        }
    }
}


__global__ void scaleObject ( cufftComplex* Object, float norm, params_t* params )
{

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;

    const int dim1 = params->IM.ObjectDim;
    const int dim2 = params->IM.ObjectDim;
    const int slices = params->IM.Slices;

    int s;
    for (int slice = 0; slice < slices; slice += 1)
    {

        sgCoord3D(s, i1, i2, slice, dim1, dim2);

        Object[s].x *= norm;
        Object[s].y *= norm;
    }

}


__global__ void copyObjectIn ( cufftComplex* ObjectOutx, cufftComplex* ObjectOuty, cufftComplex* ObjectIn, params_t* params )
{

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;

    const int l1 = params->IM.ObjectDim;
    const int l2 = params->IM.ObjectDim;
    const int Slices = params->IM.Slices;

    int s;
    for (int slice = 0; slice < Slices; slice += 1)
    {

        sgCoord3D(s, i1, i2, slice, l1, l2);

        ObjectOutx[s].x = ObjectIn[s].x;
        ObjectOuty[s].x = ObjectIn[s].y;
    }

}


__global__ void copyObjectOut ( cufftComplex* ObjectOut, cufftComplex* ObjectInx, cufftComplex* ObjectIny, params_t* params )
{

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;

    const int l1 = params->IM.ObjectDim;
    const int l2 = params->IM.ObjectDim;
    const int Slices = params->IM.Slices;

    int s;
    for (int slice = 0; slice < Slices; slice += 1)
    {

        sgCoord3D(s, i1, i2, slice, l1, l2);

        ObjectOut[s].x = ObjectInx[s].x;
        ObjectOut[s].y = ObjectIny[s].x;
    }

}


__global__ void addObjectOut ( cufftComplex* ObjectOut, cufftComplex* ObjectInx, cufftComplex* ObjectIny, float fctr, params_t* params )
{

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;

    const int dim1 = params->IM.ObjectDim;
    const int dim2 = params->IM.ObjectDim;
    const int Slices = params->IM.Slices;

    int s;
    for (int slice = 0; slice < Slices; slice += 1)
    {

        sgCoord3D(s, i1, i2, slice, dim1, dim2);

        atomicAdd(&(ObjectOut[s].x), fctr * ObjectInx[s].x ); 
        atomicAdd(&(ObjectOut[s].y), fctr * ObjectIny[s].x ) ; 

    }

}


__global__ void probeObjectInteraction ( cuComplex* f0, cufftComplex* f1, params_t* params, int j, int flag )
{
    const int m1 = params->IM.ProbeDim;
    const int m2 = params->IM.ProbeDim;
    const int Slices = params->IM.Slices;

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;
    int i3 = ((Slices + 3) * blockIdx.x)+j;

    int s,t;
    sgCoord3D(s, i1, i2, i3, m1, m2);
    sgCoord3D(t, i1, i2, blockIdx.x, m1, m2);

    if ((i1 > -1) && (i1 < m1) && (i2 > -1) && (i2 < m2 ))
    {
        if(flag==1)
        {
            float a, b, c, d;
            a = f0[t].x;
            b = f0[t].y;
            c = f1[s].x;
            d = f1[s].y;
            float k;
            k = a * (c + d);
            d *= a + b;
            c *= b - a;
            f0[t].x = k - d;
            f0[t].y = k + c;
        }

        if(flag==0)
        {
            float a, b, c, d;
            a = f0[s].x;
            b = f0[s].y;
            c = f1[s].x;
            d = f1[s].y;
            float k;
            k = a * (c + d);
            d *= a + b;
            c *= b - a;
            f0[s].x = k - d;
            f0[s].y = k + c;
        }

        if(flag==-1)
        {
            float a, b, c, d;
            a = f0[s].x;
            b = f0[s].y;
            c = f1[t].x;
            d = f1[t].y;
            float k;
            k = a * (c + d);
            d *= a + b;
            c *= b - a;
            f0[s].x = k - d;
            f0[s].y = k + c;
        }
    }
}

__global__ void batchedCaxpy( cuComplex* f0, cufftComplex* f1, float beta, params_t* params, int j, int flag)
{
    const int m1 = params->IM.ProbeDim;
    const int m2 = params->IM.ProbeDim;

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;

    int s,t;
    sgCoord(s,i1,i2,m1);
    sgCoord3D(t,i1,i2,blockIdx.x, m1,m2);

    if(flag==1)
    {
        if ((i1 > -1) && (i1 < m1) && (i2 > -1) && (i2 < m2 ))
        {
            f1[t].x = beta * f1[t].x;
            f1[t].y = beta * f1[t].y;

            atomicAdd(&(f0[s].x), f1[t].x);
            atomicAdd(&(f0[s].y), f1[t].y);
        }
    }

    if(flag==0)
    {
        if ((i1 > -1) && (i1 < m1) && (i2 > -1) && (i2 < m2 ))
        {

            atomicAdd(&(f0[s].x), (beta*f1[s].x));
            atomicAdd(&(f0[s].y), (beta*f1[s].y));
        }
    }

    if(flag==-1)
    {
        if ((i1 > -1) && (i1 < m1) && (i2 > -1) && (i2 < m2 ))
        {
            f1[s].x = beta * f1[s].x;
            f1[s].y = beta * f1[s].y;

            atomicAdd(&(f0[t].x), f1[s].x);
            atomicAdd(&(f0[t].y), f1[s].y);
        }
    }

}

__global__ void FFTShift(cufftComplex* f0, int m1, int m2, int batchSize)
{
	const int j = blockIdx.x * blockDim.x + threadIdx.x;

	if (j < m1 * m2 * batchSize)
	{
		unsigned int i1, i2, index;
		float sgn;
		dbCoord(i1, i2, j, m1);

		index=i1+i2;
		sgn = powf(-1.f,index); 

		f0[j].x *= sgn;
		f0[j].y *= sgn;
	}
}

__global__ void doIntensity_d(cufftComplex* I_d, cufftComplex* psi, int size)
{
        // Takes the values of one layer of the object and writes the intensity in the SAME layer.

        const int i = blockIdx.x * blockDim.x + threadIdx.x;

        if (i < size) {
                float psiRe = psi[i].x;
                float psiIm = psi[i].y;
                I_d[i].x = psiRe * psiRe + psiIm * psiIm;
                I_d[i].y = 0.f;
        }
}

__global__ void undoIntensity_d( cufftComplex* f0, cufftComplex* f1, params_t* params, int j )
{

    const int m1 = params->IM.ProbeDim;
    const int m2 = params->IM.ProbeDim;
    const int Slices = params->IM.Slices;

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;
    int i3 = ((Slices + 3) * blockIdx.x) + j;
    int i3p = ((Slices + 3) * blockIdx.x) + (j + 1);

    int s,t;
    sgCoord3D(s,i1,i2,i3,m1,m2);
    sgCoord3D(t,i1,i2,i3p,m1,m2);

    if ((i1 > -1) && (i1 < m1) && (i2 > -1) && (i2 < m2 ))
    {
        float f0Re = f0[t].x;
        float f0Im = f0[t].y;
        float f1Re = f1[s].x;
        float f1Im = -f1[s].y;
        float temp = f0Re * (f1Re + f1Im);
        f1Im = f1Im * (f0Re + f0Im);
        f1Re = f1Re * (f0Im - f0Re);
        f0[s].x = temp - f1Im;
        f0[s].y = temp + f1Re;
    }
}

__global__ void initialValuesComplex(cuComplex* V, int size, float initRe, float initIm)
{

	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if (i < size) {
		V[i].x = initRe;
		V[i].y = initIm;
	}
}


__global__ void initialValuesFloat(float* V, int size, float init)
{

    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < size) {
        V[i] = init;
    }
}

__global__ void initialValuesArray( cufftComplex* fin, int dimX, int dimY, int dimZ)
{
    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;
    int i3 = (dimZ * blockIdx.x);

    int index;

    for (int slice = 0; slice < dimZ; slice ++)
    {
        sgCoord3D(index,i1,i2,i3, dimX, dimY);

        if ((i1 > -1) && (i1 < dimX ) && (i2 > -1) && (i2 < dimY ))
        {
            fin[index].x = 0.f;
            fin[index].y = 0.f;
        }

        i3 +=1;
    }
}

__global__ void sumValues_helper(cuComplex* dst, cuComplex* src, cuComplex thold, int sizeSrc, const int flag, const float mu0)
{
	// flag == 0 for regular sum,
	// flag == 1 for the sum of the absolute values

	extern __shared__ cuDoubleComplex sum[];
	int i = blockIdx.x*blockDim.x + threadIdx.x;

	if (i < sizeSrc)
	{
        if (flag == 1) // it's a sum of absolute values
        {
            sum[threadIdx.x].x = (double)absValue(src[i].x, mu0);
            sum[threadIdx.x].y = (double)absValue(src[i].y, mu0);
        }
        else // if flag == 0 it's a normal sum
        {
            sum[threadIdx.x].x = (double)src[i].x;
            sum[threadIdx.x].y = (double)src[i].y;
        }
	}
	else
	{
		sum[threadIdx.x].x = 0.0;
		sum[threadIdx.x].y = 0.0;
	}
	__syncthreads();

	int n = blockDim.x / 2;
	while (n > 1)
	{
		if (threadIdx.x < n)
		{
			sum[threadIdx.x].x += sum[threadIdx.x + n].x;
			sum[threadIdx.x].y += sum[threadIdx.x + n].y;
		}
		__syncthreads();
		n /= 2;
	}

	if (threadIdx.x == 0)
	{
		dst[blockIdx.x].x = (float)(sum[0].x + sum[1].x);
		dst[blockIdx.x].y = (float)(sum[0].y + sum[1].y);
	}
}


__global__ void sumWedgeValues_helper(cufftComplex* dst, cufftComplex* src, float* src2, cuComplex thold, int sizeSrc)
{
        // flag == 0 for regular sum,
        // flag == 1 for the um of the absolute values

        extern __shared__ cuDoubleComplex sum[];
        int i = blockIdx.x*blockDim.x + threadIdx.x;

        if (i < sizeSrc)
        {
            sum[threadIdx.x].x = (double)absValue(src[i].x * src2[i], 1.f);
            sum[threadIdx.x].y = (double)absValue(src[i].y * src2[i], 1.f);
        }
        else
        {
                sum[threadIdx.x].x = 0.0;
                sum[threadIdx.x].y = 0.0;
        }
        __syncthreads();

        int n = blockDim.x / 2;
        while (n > 1)
        {
                if (threadIdx.x < n)
                {
                        sum[threadIdx.x].x += sum[threadIdx.x + n].x;
                        sum[threadIdx.x].y += sum[threadIdx.x + n].y;
                }
                __syncthreads();
                n /= 2;
        }

        if (threadIdx.x == 0)
        {
                dst[blockIdx.x].x = (float)(sum[0].x + sum[1].x);
                dst[blockIdx.x].y = (float)(sum[0].y + sum[1].y);
        }
}


__global__ void addDerivativeAbsValues(cuComplex* dst, cuComplex* src, float fctr, int size, float mu0)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if (i < 2 * size)
	{
		const int i0 = i / 2;

		if ((i % 2) == 0)
		{
			float srcx = src[i0].x;
			if (!(srcx < 0.f))
			{
				mu0 = 1.f;
			}

			dst[i0].x += fctr * mu0 * mu0 * srcx / absValue(srcx, mu0);
		}
		else
		{
			float srcy = src[i0].y;
			if (!(srcy < 0.f))
			{
				mu0 = 1.f;
			}

			dst[i0].y += fctr * mu0 * mu0 * srcy / absValue(srcy, mu0);
		}
	}
}


__global__ void createWedgeDerivative(cufftComplex* Vkx, cufftComplex* Vky, float* W, params_t* params)
{
        int k1 = threadIdx.z + blockDim.z *blockIdx.z;
        int k2 = threadIdx.y + blockDim.y *blockIdx.y;
        int k;
        int dim1 = params->IM.ObjectDim;
        int dim2 = params->IM.ObjectDim;

        for(int k3 = 0; k3 < params->IM.Slices; k3++)
        {
            sgCoord3D(k,k1,k2,k3,dim1,dim2);

            if (absValue(W[k] * Vkx[k].x, 1.f) == 0.f)
            {
                Vkx[k].x = 0.f;
		Vkx[k].y = 0.f;
            }
            else
            {
                Vkx[k].x = W[k] * W[k] * Vkx[k].x / absValue( W[k] * Vkx[k].x, 1.f); // W x sign(W x V)
		Vkx[k].y = 0.f;
            }
            if (absValue(W[k] * Vky[k].x, 1.f) == 0.f)
            {
                Vky[k].x = 0.f;
		Vky[k].y = 0.f;
            }
            else
            {
                Vky[k].x = W[k] * W[k] * Vky[k].x / absValue( W[k] * Vky[k].x, 1.f); // W x sign(W x V)
		Vky[k].y = 0.f;
            }
        }
}


__device__ float absValue(float x, const float mu0)
{
	float y, epsSq;
	epsSq = 1e-8;

	if (x < 0.f)
	{
		x *= mu0;
	}

	y = sqrtf(x * x + epsSq);

	return (y);
}

float sumAbsValues(cuComplex* V, const int size, const float mu0)
{
	cuComplex dummyThold, sumV;
	dummyThold.x = -FLT_MAX;
	dummyThold.y = -FLT_MAX;

	sumV = sumAbsValues_helper(V, dummyThold, size, 1, mu0); // flag is 1 for a sum of absolute values

	return(sumV.x + sumV.y);
}

cuComplex sumAbsValues_helper(cuComplex* V, cuComplex thold, const int size, const int flag, const float mu0)
{
	// flag == 0 for regular sum,
	// flag == 1 for the sum of the absolute values

	const int bS = 256; // Make it high to reduce memory load (Maximum allowed on Tesla K20c: 1024). MUST be a power of 2 (for this application).
	int vecSize = size;
	int sumSize0 = vecSize / bS + 1;
	cuComplex *sum0;
	cuda_assert(cudaMalloc((void**)&sum0, sumSize0 * sizeof(cuComplex)));

	sumValues_helper <<< sumSize0, bS, bS * sizeof(cuDoubleComplex) >>> (sum0, V, thold, vecSize, flag, mu0);
	cuda_assert(cudaDeviceSynchronize());

	while (sumSize0 > 1)
	{
		vecSize = sumSize0;
		sumSize0 = sumSize0 / bS + 1;
		// use the regular sum ( flag == 0 ) here; the operation above has applied the abs val or the threshold already irreversibly to sum0
		sumValues_helper <<< sumSize0, bS, bS * sizeof(cuDoubleComplex) >>> (sum0, sum0, thold, vecSize, 0, 1.f);
		cuda_assert(cudaDeviceSynchronize());
	}

	cuComplex sum1;
	cuda_assert(cudaMemcpy(&sum1, sum0, sizeof(cuComplex), cudaMemcpyDeviceToHost));

	cuda_assert(cudaFree(sum0));

	return(sum1);
}

float sumWedgeAbsValues(cufftComplex* Vk, float* W, const int size)
{
        cuComplex dummyThold, sumV;
        dummyThold.x = -FLT_MAX;
        dummyThold.y = -FLT_MAX;

        sumV = sumWedgeAbsValues_helper(Vk, W, dummyThold, size);

        return(sumV.x + sumV.y);
}

cuComplex sumWedgeAbsValues_helper(cufftComplex* Vk, float* W, cuComplex thold, const int size)
{
        const int bS = 256; // Make it high to reduce memory load (Maximum allowed on Tesla K20c: 1024). MUST be a power of 2 (for this application).
        int vecSize = size;
        int sumSize0 = vecSize / bS + 1;
        cuComplex *sum0;
        cuda_assert(cudaMalloc((void**)&sum0, sumSize0 * sizeof(cuComplex)));

        sumWedgeValues_helper <<< sumSize0, bS, bS * sizeof(cuDoubleComplex) >>> (sum0, Vk, W, thold, vecSize);
        cuda_assert(cudaDeviceSynchronize());

        while (sumSize0 > 1)
        {
                vecSize = sumSize0;
                sumSize0 = sumSize0 / bS + 1;
                // use the regular sum ( flag == 0 ) here; the operation above has applied the abs val or the threshold already irreversibly to sum0
                sumValues_helper <<< sumSize0, bS, bS * sizeof(cuDoubleComplex) >>> (sum0, sum0, thold, vecSize, 0, 1.f);
                cuda_assert(cudaDeviceSynchronize());
        }

        cuComplex sum1;
        cuda_assert(cudaMemcpy(&sum1, sum0, sizeof(cuComplex), cudaMemcpyDeviceToHost));

        cuda_assert(cudaFree(sum0));

        return(sum1);
}

__global__ void fillIn(cufftComplex* V, params_t* params_d)
{
    const int l1 = params_d->IM.ObjectDim;
    const int l2 = params_d->IM.ObjectDim;
    const int Slices = params_d->IM.Slices;

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;

    int s;
    for (int i3 = 0; i3 < Slices; i3 ++)
    {
	sgCoord3D(s,i1,i2,i3,l1,l2);

	if(s==0)
	{ V[s].x = 1.f;}
    }
}

