/*------------------------------------------------
	Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
	See ROP.cu for full notice
------------------------------------------------*/

#ifndef initialization_H
#define initialization_H

#include <cufft.h>
#include <cublas_v2.h>
#include <thrust/device_vector.h>
#include <string>

bool fexists(char const* filename);

void checkArgs(int argc, char* argv[]);

struct cst_t {
    float m0; // = 9.109389e-31;  // electron rest mass (kg)
    float c;  // = 299792458.0f;   // speed of light (m/s)
    float e;  // = 1.602177e-19;   // elementary charge (C)
    float h;  // = 6.626075e-34;   // Planck's constant (Js)
    float pi; // = 3.1416 (dimensionless)
};

struct aberration_t {
    float C1_0;  // defocus (m)
    float C1_1;  // angle of defocus (rad)
    float A1_0;  // magnitude of astigmatism (m)
    float A1_1;  // angle of astigmatism (rad)
    float A2_0;
    float A2_1;
    float B2_0;
    float B2_1;
    float C3_0;
    float C3_1;
    float A3_0;
    float A3_1;
    float S3_0;
    float S3_1;
    float A4_0;
    float A4_1;
    float B4_0;
    float B4_1;
    float D4_0;
    float D4_1;
    float C5_0;
    float C5_1;
    float A5_0;
    float A5_1;
    float R5_0;
    float R5_1;
    float S5_0;
    float S5_1;
};

struct EM_t {
    float E0;                 // acceleration voltage (V)
    float gamma;              // From relativity theory: 1 + e*E0/m0/c^2;
    float lambda;             // electron wavelength (m), see De Graef p. 92
    float sigma;              // interaction constant (1/(Vm))
    aberration_t aberration;  // Microscope aberrations
    float defocspread;        // Temporal incoherence  Defocus spread for the temporal incoherence (m)
    float illangle;           // illumination half angle characterizing the spatial incoherence (rad)
    float condensorAngle; // Half-angle of convergence of the illumination
    float mtfa;               // parameters of the MTF, see my article in Micros and microanalysis
    float mtfb;
    float mtfc;
    float mtfd;
    float ObjAp;              // DIAMETER (!!! (I'm so sorry...)) of the objective aperture (rad)
};

struct IM_t {
    int ObjectDim;
    int ProbeDim;           // 1st dimension of the reconstructed potential m1 = n1 + 2*dn1
    int CBEDDim;           // 1st dimension of the measurements
    int Slices;           // 3rd dimension of the reconstructed potential, i.e. number of slices
    int equalSlice;	//set slices equal
    float PixelSize;         // pixelsize in 1st dimension of measurements and reconstruction (m)
    float SliceDistance;         // pixelsize in 3rd dimension of measurements and reconstruction (m)
    int dn;          // additional edge around reconstruction in 1st dimension
    int batchSize;
};

struct SCN_t {
    int ptyMode;
    int ScanX;       // 2nd dimension of the scan [pix]
    int ScanY;       // 1st dimension of the scan [pix]
    float ScanDistanceX;      // Sampling length [m]
    float ScanDistanceY;      // Sampling length [m]
    int positions;
    thrust::device_vector<float> BeamPos;
    float* BeamPosRaw;
};


struct RE_t {
    float pftr;           // Threshold for the potential flipping
    float pftr0_x;        // Absolute value for pftr for real part of potential, for internal use
    float pftr0_y;        // Absolute value for pftr for imaginary part of potential, for internal use
    bool dopf;            // true or false, whether to do the potential flipping
    float mu;             // Relative importance of the L1-norm, put to zero if no L1 desired
    float beta_wedge;	  // Weighting parameter of the missing wedge regularization
    float sphere_radius;
    float gauss_sigma;
    float alpha;
    int alpha_its;
    float beta;
    int beta_its;
    float gamma;
    int gamma_its;
    int its;              // Total number of iterations
    int binning;              // binning
    thrust::host_vector<float> E_h;
    thrust::host_vector<float> L1_h;
    int ErrorFunction;    // Kind of error function. 0: Least squares. 1: Weighted least squares
    int maskFlag;         // Which filter to use on the raw reconstruction. 0 none, 1 2DRingfilter
    float cgprOrthTest;   // Threshold value for the test if to consecutive gradients in CG-PR are orthogonal enough
    float cgC1;           // Parameter of the strong Wolfe conditions for sufficient descent of the function value
    float cgC2;           // Parameter of the strong Wolfe conditions for sufficient descent of the function derivative
    float negAbsFctr;     // Factor with which to multiply the absolute values of negative numbers in the solution
    float defocPoly0;     // zeroth-order term of the second order polynomial fitted to defoci
    float defocPoly1;     // First-order term of the second order polynomial fitted to defoci
    float defocPoly2;     // Second-order term of the second order polynomial fitted to defoci
    int noNuisParams;     // Number of nuissance params to estimate
};

struct CU_t {
    int gS;
    int gS2D;
    int gS2D_l;
    int gSB;
    int bS;

    dim3 gST; // grid size for batched m dimension
    dim3 bST;

    int DRAM; // Put here all the stuff that you retrieve from the DeviceProp function!
    int maxGrid;
    int maxThreads;

    cufftHandle cufftPlan;
    cufftHandle cufftPlan_l;
    cufftHandle cufftPlan3D;
    cufftHandle cufftPlanMany;
    cublasHandle_t cublasHandle;
};

struct params_t {
    cst_t cst;  // Nature's constants
    EM_t EM;    // Electron microscope parameters
    IM_t IM;    // Imaging parameters
    SCN_t SCN;  // Scanning parameters
    RE_t RE;    // Reconstruction parameters
    CU_t CU;    // CUDA parameters
};

class Parameters{

private:
    std::string paramsFileName; //Parameter filename
    cudaDeviceProp prop; //GPU-device properties

public:
    params_t* params; // Parameters_HOST
    params_t* params_d; // Parameters_DEVICE

    //Default Constructor
    Parameters();
    //Constructor
    Parameters(std::string name);
    //Destructor
    ~Parameters();

    void defaultParams( params_t* params );

    void readParams(std::string fileName, params_t* params );

    void setGridAndBlockSize( params_t* params, cudaDeviceProp prop );

    void copyParams ( params_t* dst, params_t* src );

    void setCufftPlan( params_t* params );

    void setCublasHandle( params_t* params );

    void consitentParams( params_t* params );

    void writeConfig( char* file, params_t* params );

};

/*
 * Array class
 */

class Array {

private:
    CU_t CU;

public:
    cufftComplex* values_d = nullptr;
    int dimX_h, dimY_h, dimZ_h, batch;
    dim3 gS, bS;

    //Default Constructor
    Array();
    //Constructor
    Array(int X, int Y, int Z, int batch_, CU_t CU_);
    //Copy Constructor
    Array(Array const& A_);
    //Copy Assignment Operator
    Array& operator=(Array const& A_);
    //Destructor
    ~Array();

    //Swap function
    void swap(Array& A_);
    void createGridAndBlockSize(); //CU_t CU
    //initialize values_d
    void initialize();
};

/*
 * Specimen class
 */

class Specimen:public Parameters
{

public:
    float *E_h, *L1_h;
    Array *V = nullptr, *dE0 = nullptr, *dE1 = nullptr, *D = nullptr;
    Array *psi = nullptr, *dpsi0 = nullptr, *dpsi1 = nullptr, *Dpsi = nullptr;
    float *dpos0, *dpos1, *Dpos;
    float *I_h = nullptr, *I_d = nullptr;

    //Default Constructor
    Specimen();
    //Constructor
    Specimen(int argc, char* argv[]);
    //Destructor
    ~Specimen();

    //Create an ideal probe according to input parameters
    void initialProbe(Array* psi, int argc, char* argv[], params_t* params, params_t* params_d);
    //Copy a probe
    void copyProbe(Array* psi, Array* psi_, params_t* params, params_t* params_d, params_t* params_D);
};

void readInitialPsi( cufftComplex* psi, char* argv[], params_t* params );

void readInitialPotential ( cufftComplex* V, char* argv[], params_t* params );

#endif
