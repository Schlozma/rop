/*------------------------------------------------
	Copyright (C) 2013 Wouter Van den Broek
	See IDES.cu for full notice
------------------------------------------------*/

#ifndef globalVariables_H
#define globalVariables_H

extern int MATLAB_TILT_COMPATIBILITY;

extern int NO_FUNCTION_EVALS_IDES;

extern int NO_DERIVATIVE_EVALS_IDES;

// extern int RESULT_SETENV;

#endif