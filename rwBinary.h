/*------------------------------------------------
        Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
        See ROP.cu for full notice
------------------------------------------------*/

#ifndef rwBinary_H
#define rwBinary_H

void readBinary(const char* filename, float* I, int size);

void writeBinary (char* filename, float* f, int size);

void appendBinary ( char* filename, float* f, int size );

#endif
