/*------------------------------------------------
	Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
	See ROP.cu for full notice
------------------------------------------------*/

#ifndef optimization_H
#define optimization_H

#include <cuComplex.h>
#include <cufft.h>
#include "initialization.h"
#include "kernel_assert.hpp"

struct obj{};
struct object
{
    using typemode = cufftComplex;
    using classmode = obj;
    int size;
    int j;
    float gamma;
    float dEnorm;
    float step;
    float* initStep;

    void reset(Specimen const& spec);

    void norm(Specimen const& spec, cufftComplex* dE1_d, float &dEnorm01);

    void copy(Specimen const& spec, cufftComplex* dE0_d, cufftComplex* dE1_d);

    void dot(Specimen const& spec, cufftComplex* dE0_d, cufftComplex* dE1_d, float &dEnorm01);

    void axpy(Specimen const& spec, cufftComplex* dE1_d, cufftComplex* D_d);

    void scal(Specimen const& spec, cufftComplex* D_d);
};

struct prb{};
struct probe
{
    using typemode = cufftComplex;
    using classmode = prb;
    int size;
    int j;
    float gamma;
    float dEnorm;
    float step;
    float* initStep;

    void reset(Specimen const& spec);

    void norm(Specimen const& spec, cufftComplex* dE1_d, float &dEnorm01);

    void copy(Specimen const& spec, cufftComplex* dE0_d, cufftComplex* dE1_d);

    void dot(Specimen const& spec, cufftComplex* dE0_d, cufftComplex* dE1_d, float &dEnorm01);

    void axpy(Specimen const& spec, cufftComplex* dE1_d, cufftComplex* D_d);

    void scal(Specimen const& spec, cufftComplex* D_d);
};


struct pos{};
struct position
{
    using typemode = float;
    using classmode = pos;
    int size;
    int j;
    float gamma;
    float dEnorm;
    float step;
    float* initStep;

    void reset(Specimen const& spec);

    void norm(Specimen const& spec, float* dE1_d, float &dEnorm01);

    void copy(Specimen const& spec, float* dE0_d, float* dE1_d);

    void dot(Specimen const& spec, float* dE0_d, float* dE1_d, float &dEnorm01);

    void axpy(Specimen const& spec, float* dE1_d, float* D_d);

    void scal(Specimen const& spec, float* D_d);
};

void updateObject(Specimen const &spec, int& j);

void updateProbe(Specimen const &spec, int& j);

void updatePositions(Specimen const &spec, int& j);

__global__ void copyMiddleOut( float* Imodel, cuComplex* psi, params_t* params );

__global__ void copyMiddleIn( cuComplex* dE, float* Imodel, params_t* params );

void optimize(Specimen const &spec);

void applyEqualSlice(Array* dV, params_t* params, params_t* params_d);

template <typename T>
float objectiveFunctionSummed(Specimen const &spec, int j);

void derivativeFunction (Specimen const& spec, Array* dEdV, Array* V, Array* dpsi_t, float* dpos_t, int batch, obj);

void derivativeFunction (Specimen const& spec, Array* dEdV, Array* V, Array* dpsi_t, float* dpos_t, int batch, prb);

void derivativeFunction (Specimen const& spec, Array* dEdV, Array* V, Array* dpsi_t, float* dpos_t, int batch, pos);

float errorFunction (Array* dE, float* I, Array* psi, params_t* params, params_t* params_d );

float errorFunction1 ( Array* dE, float* I, Array* psi, params_t* params, params_t* params_d );

float errorFunction2 ( Array* dE, float* I, Array* psi, params_t* params, params_t* params_d);

float errorFunction3 ( Array* dE, float* I, Array* psi, params_t* params, params_t* params_d );

float regularizationFunction ( cuComplex* dEdV, cuComplex* V, params_t* params, params_t* params_d );

__global__ void missingWedge(float* W, params_t* params_d);

__global__ void missingSphere(float* W, params_t* params_d);

__global__ void missingGauss(float* W, params_t* params_d);

float stepSize (cuComplex* V, float alpha0, float E0, cuComplex* dEdV, float* I, float* dNuis, params_t* params, params_t* params_d, float* xyBCoord, int j, int k, int helper);

template <typename T>
float lineSearchCG ( float* E_h, Specimen const &spec, typename T::typemode* dE1_d, typename T::typemode* D_d, T* tag, int j );

void takeStepCG (Specimen const &spec, cufftComplex* dEdV, float alpha, object* tag);

void takeStepCG (Specimen const &spec, cufftComplex* dEdV, float beta, probe* tag);

void takeStepCG( Specimen const &spec, float* dpos, float gamma, position* tag);

float minimumThirdOrderPoly( float x0, float x1, float f0, float f1, float dF0, float dF1 );

float minimumSecondOrderPoly( float x0, float x1, float f0, float f1, float dF0);

void saveResults(cufftComplex* V, cufftComplex* psi, float* E, float* L1, int j, params_t* params);

void saveModelMeasurements(Array* V, Array* psi, params_t* params, params_t* params_d, int j);

template <typename T>
float searchDirectionPR( Specimen const &spec, typename T::typemode* dE0_d, typename T::typemode* dE1_d, typename T::typemode* D_d, T* tag);

#endif
