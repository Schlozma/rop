#ifndef kernel_assert_HPP
#define kernel_assert_HPP

#include "cuda_assert.hpp"

#ifdef kernel_assert
#undef kernel_assert 
#endif

#define kernel_assert(x) \
        do \
        { \
            x; \
            cuda_assert( cudaGetLastError() ); \
        } \
        while(0)

#endif

