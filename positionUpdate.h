/*------------------------------------------------
        Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
        See ROP.cu for full notice
------------------------------------------------*/

#ifndef positionUpdate_H
#define positionUpdate_H

__global__ void derivative_x_real(cufftComplex *f, cufftComplex *df, int m1, int m2);

__global__ void derivative_x_imag(cufftComplex *f, cufftComplex *df, int m1, int m2);

__global__ void derivative_y(cufftComplex *f, cufftComplex *df, int m1, int m2);

void probeDerivative(cufftComplex* psi, cufftComplex* dpsi, params_t* params, int dimension);

void updateProbePositions(Specimen const& spec, Array* Dpsi, float* tmpDpos, int batchI);

#endif
