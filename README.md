# Regularized Optimization for Ptychography (ROP)
---

ROP is a multislice-capable conjugate gradient descent ptychographic reconstruction algorithm that can reconstruct the phase while simultaneously retrieving the probe shape and positions. The reconstruction algorithm adapts the design of an Artificial Neural Network (ANN), a model very well established in the machine learning community, to efficiently calculatethe gradients and the reconstructed phase, which can be regularized during optimization. A schematic of the network model is shown in the figure below.

<p align="center">
 <img src="./misc/images/ROP_Network.png" alt="Drawing", width=30%>
</p>


### System Requirement

+ software
    + gcc (7.4.0)
    + g++ (7.4.0)
    + CUDA (10.0)

+ recommanded hardware
    + CPUs: Intel(R) Xeon(R) Gold 6136 @ 3.00GHz, 24 Cores; 
    + GPUs: NVIDIA V100 16GB/32GB, 5120 Cores
    + 2T hard disk space
    + 187 GB Memory

### Demo Tutorial
For people who are interested in applying ROP to their own data, check out the [ROP-DEMO](https://gitlab.com/schlozma/rop-demo) repo.

### bibtex

We kindly ask you to cite our publication if you use our dataset, code or models in your work.

```
@article{schloz2020overcoming,
  title={Overcoming information reduced data and experimentally uncertain parameters in ptychography with regularized optimization},
  author={Schloz, Marcel and Pekin, Thomas Christopher and Chen, Zhen and Van den Broek, Wouter and Muller, David Anthony and Koch, Christoph Tobias},
  journal={Optics Express},
  volume={28},
  number={19},
  pages={28306--28323},
  year={2020},
  publisher={Optical Society of America}
}
```


### Copyright
Copyright (c), 2019, Marcel Schloz* and Wouter Van den Broek*,
*Humboldt Universitaet zu Berlin, Berlin.
All rights reserved.

Redistribution to third parties is not allowed. Use in source and binary forms,
with or without modification, is permitted provided that the following
conditions are met:

    - Modifications and copies of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Modifications and copies in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    - Neither the name of the Humboldt Universitaet zu Berlin nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

This software is made in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
The use of this software is limited to ACADEMIC, NON-COMMERCIAL purposes only.
The authors of this software do not accept any liability, nor warrant that the use of
this software will not infringe any third-party rights.

