/*------------------------------------------------
	Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
	See ptyIDES.cu for full notice
------------------------------------------------*/

#include <cstdio>

void readBinary (const char* filename, float* I, int size )
{
    FILE* fr;
    fr = fopen ( filename , "rb" );

    fread ( ( void* ) I, sizeof ( float ), size, fr );
    fclose ( fr );
}

void writeBinary ( char* filename, float* f, int size )
{
    FILE* fw;
    fw = fopen ( filename, "wb" );

    fwrite ( ( const void* ) f, sizeof ( float ), size, fw );
    fclose ( fw );
}

void appendBinary ( char* filename, float* f, int size )
{
    FILE* fw;
    fw = fopen ( filename, "ab" );

    fwrite ( ( const void* ) f, sizeof ( float ), size, fw );
    fclose ( fw );
}
