#include <cstdio>
#include "cuda_assert.hpp"

#include "initialization.h"
#include "optimization.h"

void showLicense ();

int main (int argc, char* argv[])
{
    showLicense ();
    cuda_assert ( cudaSetDevice ( 0 ) );
    checkArgs(argc, argv);

    fprintf ( stderr, "\n  Starting ROP.\n" );

    //Read Parameters and Data
    //Create Specimen object
    Specimen Sample(argc, argv);
    //Perform Optimization
    optimize(Sample);

    fprintf ( stderr, "  Done.\n" );

    return 0;
}

void showLicense()
{
	fprintf(stderr, "\nCopyright (c), 2019, Marcel Schloz* and Wouter Van den Broek*,\n");
	fprintf(stderr, "*Humboldt Universitaet zu Berlin, Berlin.\n");
	fprintf(stderr, "All rights reserved.\n\n");

	fprintf(stderr, "Redistribution to third parties is not allowed. Use in source and binary forms,\n");
	fprintf(stderr, "with or without modification, is permitted provided that the following\n");
	fprintf(stderr, "conditions are met:\n");
	fprintf(stderr, "    - Modifications and copies of source code must retain the above copyright\n");
	fprintf(stderr, "      notice, this list of conditions and the following disclaimer.\n");
	fprintf(stderr, "    - Modifications and copies in binary form must reproduce the above copyright\n");
	fprintf(stderr, "      notice, this list of conditions and the following disclaimer in the\n");
	fprintf(stderr, "      documentation and/or other materials provided with the distribution.\n");
	fprintf(stderr, "    - Neither the name of the Humboldt Universitaet zu Berlin nor the\n");
	fprintf(stderr, "      names of its contributors may be used to endorse or promote products\n");
	fprintf(stderr, "      derived from this software without specific prior written permission.\n\n");

	fprintf(stderr, "This software is made in the hope that it will be useful, but WITHOUT ANY WARRANTY;\n");
	fprintf(stderr, "without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n");
	fprintf(stderr, "The use of this software is limited to ACADEMIC, NON-COMMERCIAL purposes only.\n");
	fprintf(stderr, "The authors of this software do not accept any liability, nor warrant that the use of\n");
	fprintf(stderr, "this software will not infringe any third-party rights.\n");
}
