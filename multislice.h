/*------------------------------------------------
	Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
	See ROP.cu for full notice
------------------------------------------------*/

#ifndef multislice_H
#define multislice_H

#include <cufft.h>
#include "initialization.h"
#include <string>

__global__ void potential2Transmission(cufftComplex* t, cufftComplex* V, int size);

__global__ void zeroHighFreq(cufftComplex* f, int dim1, int dim2, float mindimsq);

__global__ void fresnelPropagatorDevice(cufftComplex* frProp, params_t* params, int flag);

__global__ void multiplyLensFunction( cufftComplex* psi, int k, params_t* params, int flag );

void shift(Array* fout, Array* fin, int scnI, params_t* params, float* BeamPos_d, int flag);

__global__ void shiftOut_d(cufftComplex* fOut, cufftComplex* fIn, int foutdimw, int findimw, int scnI, params_t* params, float* BeamPos_d);

__global__ void shiftIn_d(cufftComplex* fOut, cufftComplex* fIn, int foutdimw, int findimw, int scnI, params_t* params, float* BeamPos_d);

void forwardPropagation(Array* psi, Array* V, params_t* params, params_t* params_d);

void backwardPropagation(Array* dEdV, Array* psi, Array* dpsi, Array* V, params_t* params, params_t* params_d);

void bandwidthLimit(cufftComplex* f, params_t* params_h);

void incomingWave( cufftComplex* psi, params_t* params, params_t* params_d );

void fresnelPropagator(cufftComplex* frProp, params_t* params_h, params_t* params_d, int flag);

void convolveWithFrProp(cufftComplex* psi, cufftComplex* frProp, params_t* params, params_t* params_d);

void farFieldTransform(Array* psi, params_t* params, params_t* params_d);

void undofarFieldTransform(Array* dEdV, params_t* params, params_t* params_d);

void doIntensity( Array* psi, params_t* params, params_t* params_d, int j);

void undoIntensity( Array* dEdV, Array* psi, params_t* params, params_t* params_d, int j);

void saveTestImageComplex(char* fileName, cufftComplex* src, int size);

void saveTestImageFloat(std::string fileName, float* src, int size);

#endif
