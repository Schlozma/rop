/*------------------------------------------------
        Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
        See ROP.cu for full notice
------------------------------------------------*/

#include <cstdio>
#include <cufft.h>
#include "initialization.h"
#include "multislice.h"
#include "rwBinary.h"
#include "optimization.h"
#include "mathematics.h"
#include "positionUpdate.h"

#include <cuda.h>
#include "cuda_assert.hpp"
#include "cufft_assert.h"
#include "cublas_assert.hpp"
#include <cublas_v2.h>
#include <cuComplex.h>
#include "coordArithmetic.h"
#include <float.h>
#include "globalVariables.h"

#include <fstream>
#include <string>

#define _USE_MATH_DEFINES

void object::reset(Specimen const& spec)
{
    gamma = 0.f;
    dEnorm = 0.f;
    step = spec.params->RE.alpha;
    initStep = ( float* ) calloc ( 3, sizeof ( float ) );
    initStep[0] = step;
    size = spec.params->IM.ObjectDim * spec.params->IM.ObjectDim * spec.params->IM.Slices;
    j = 0;
}

void object::norm(Specimen const& spec, cufftComplex* dE1_d, float &dEnorm1)
{
    cublas_assert(cublasScnrm2(spec.params->CU.cublasHandle, size, dE1_d, 1, &dEnorm1));
    cuda_assert (cudaDeviceSynchronize());
}

void object::copy(Specimen const& spec, cufftComplex* dE1_d, cufftComplex* dE0_d)
{
    cublas_assert( cublasCcopy( spec.params->CU.cublasHandle, size, dE1_d, 1, dE0_d, 1 ) );
    cuda_assert ( cudaDeviceSynchronize () );
}

void object::dot(Specimen const& spec, cufftComplex* dE0_d, cufftComplex* dE1_d, float &dEnorm01)
{
    cublas_assert(cublasSdot(spec.params->CU.cublasHandle, 2 * size, (float *) dE0_d, 1, (float *) dE1_d, 1, &dEnorm01));
    cuda_assert (cudaDeviceSynchronize());
}

void object::axpy(Specimen const& spec, cufftComplex* dE1_d, cufftComplex* D_d)
{
    cufftComplex one;
    one.x = 1.0f;
    one.y = 0.0f;
    cublas_assert(cublasCaxpy(spec.params->CU.cublasHandle, size, &one, dE1_d, 1, D_d, 1));
    cuda_assert (cudaDeviceSynchronize());
}

void object::scal(Specimen const& spec, cufftComplex* D_d)
{
    cublas_assert(cublasCsscal(spec.params->CU.cublasHandle, size, &gamma, D_d, 1));
    cuda_assert (cudaDeviceSynchronize());
}

void probe::reset(Specimen const& spec)
{
    gamma = 0.f;
    dEnorm = 0.f;
    step = spec.params->RE.beta;
    initStep = ( float* ) calloc ( 3, sizeof ( float ) );
    initStep[0] = step;
    size = spec.params->IM.ProbeDim * spec.params->IM.ProbeDim;
    j = 0;
}

void probe::norm(Specimen const& spec, cufftComplex* dE1_d, float &dEnorm1)
{
    cublas_assert(cublasScnrm2(spec.params->CU.cublasHandle, size, dE1_d, 1, &dEnorm1));
    cuda_assert (cudaDeviceSynchronize());
}

void probe::copy(Specimen const& spec, cufftComplex* dE1_d, cufftComplex* dE0_d)
{
    cublas_assert( cublasCcopy( spec.params->CU.cublasHandle, size, dE1_d, 1, dE0_d, 1 ) );
    cuda_assert ( cudaDeviceSynchronize () );
}

void probe::dot(Specimen const& spec, cufftComplex* dE0_d, cufftComplex* dE1_d, float &dEnorm01)
{
    cublas_assert(cublasSdot(spec.params->CU.cublasHandle, 2 * size, (float *) dE0_d, 1, (float *) dE1_d, 1, &dEnorm01));
    cuda_assert (cudaDeviceSynchronize());
}

void probe::axpy(Specimen const& spec, cufftComplex* dE1_d, cufftComplex* D_d)
{
    cufftComplex one;
    one.x = 1.0f;
    one.y = 0.0f;
    cublas_assert(cublasCaxpy(spec.params->CU.cublasHandle, size, &one, dE1_d, 1, D_d, 1));
    cuda_assert (cudaDeviceSynchronize());
}

void probe::scal(Specimen const& spec, cufftComplex* D_d)
{
    cublas_assert(cublasCsscal(spec.params->CU.cublasHandle, size, &gamma, D_d, 1));
    cuda_assert (cudaDeviceSynchronize());
}

void position::reset(Specimen const& spec)
{
    gamma = 0.f;
    dEnorm = 0.f;
    step = spec.params->RE.gamma;
    initStep = ( float* ) calloc ( 3, sizeof ( float ) );
    initStep[0] = step;
    size = spec.params->SCN.positions;
    j = 0;
}

void position::norm(Specimen const& spec, float* dE1_d, float &dEnorm1)
{
    cublas_assert( cublasSnrm2( spec.params->CU.cublasHandle, size, dE1_d, 1, &dEnorm1 ) );
    cuda_assert ( cudaDeviceSynchronize () );
}

void position::copy(Specimen const& spec, float* dE1_d, float* dE0_d)
{
    cublas_assert( cublasScopy( spec.params->CU.cublasHandle, size, dE1_d, 1, dE0_d, 1 ) );
    cuda_assert ( cudaDeviceSynchronize () );
}

void position::dot(Specimen const& spec, float* dE0_d, float* dE1_d, float &dEnorm01)
{
    cublas_assert( cublasSdot( spec.params->CU.cublasHandle, size, dE0_d, 1, dE1_d, 1, &dEnorm01 ) );
    cuda_assert ( cudaDeviceSynchronize () );
}

void position::axpy(Specimen const& spec, float* dE1_d, float* D_d)
{
    float one = 1.f;
    cublas_assert(cublasSaxpy(spec.params->CU.cublasHandle, size, &one, dE1_d, 1, D_d, 1));
    cuda_assert (cudaDeviceSynchronize());
}

void position::scal(Specimen const& spec, float* D_d)
{
    cublas_assert(cublasSscal(spec.params->CU.cublasHandle, size, &gamma, D_d, 1));
    cuda_assert (cudaDeviceSynchronize());
}

__global__ void copyMiddleOut( float* Imodel, cuComplex* psi, params_t* params )
{
    const int m1 = params->IM.ProbeDim;
    const int m2 = params->IM.ProbeDim;
    const int n1 = params->IM.CBEDDim;
    const int n2 = params->IM.CBEDDim;
    const int dn = params->IM.dn;

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;
    int i3 = (params->IM.Slices + 3) * blockIdx.x + (params->IM.Slices + 2);

    int k;
    sgCoord3D(k, i1, i2, i3, m1, m2);

    float temp = 0.f;

    i1=i1-dn;
    i2=i2-dn;

    if ((i1 > -1) && (i1 < n1) && (i2 > -1) && (i2 < n2 ))
    {
        int m;
        sgCoord3D(m, i1, i2, blockIdx.x , n1, n2);

        temp= psi[k].x;
        Imodel[m] = temp;
    }
}

__global__ void copyMiddleIn( cuComplex* dE, float* Imodel, params_t* params )
{

    // flag == 1: forward rotation, flag == -1: rotate back
    const int m1 = params->IM.ProbeDim;
    const int m2 = params->IM.ProbeDim;
    const int n1 = params->IM.CBEDDim;
    const int n2 = params->IM.CBEDDim;
    const int dn = params->IM.dn;

    int i1 = threadIdx.z + blockDim.z *blockIdx.z;
    int i2 = threadIdx.y + blockDim.y *blockIdx.y;
    int i3 = (params->IM.Slices + 3) * blockIdx.x + (params->IM.Slices + 2);

    int m;
    sgCoord3D(m, i1, i2, blockIdx.x , n1, n2);

    int k;
    float temp = 0.f;

    i1=i1+dn;
    i2=i2+dn;

    if ((i1 > -1) && (i1 < (n1 + dn)) && (i2 > -1) && (i2 < (n2 + dn)))
    {
        sgCoord3D(k, i1, i2, i3, m1, m2);
        temp = Imodel[m];
        dE[k].x=temp;
    }

}

void optimize(Specimen const &spec)
{
    int j = 0;
    while (j < ( spec.params->RE.its - 1 ) )
    {
        if( spec.params->RE.alpha > 1e-17f  )
        {
            //Update object
            updateObject(spec, j);
        }
        if( spec.params->RE.beta > 1e-17f  )
        {
            //Update probe shape
            updateProbe(spec, j);
        }
        if( spec.params->RE.gamma > 1e-17f  )
        {
            //Update probe positions
            updatePositions(spec, j);
        }
        
        //Save process once in a while
        saveResults ( spec.V->values_d, spec.psi->values_d, spec.E_h, spec.L1_h, j, spec.params );
        saveModelMeasurements( spec.V, spec.psi, spec.params, spec.params_d, j);
        fprintf(stderr, "  Iteration %i.\n", j+1);
    }

}

void updateObject(Specimen const &spec, int& j)
{
    std::cout<<" \n ________________ Object Update ________________ \n" << std::endl;
    object* tag = new object;
    tag->reset(spec);
    //Determine object derivative
    spec.E_h[tag->j] = objectiveFunctionSummed<object>(spec, tag->j);
    //Do conjugate gradient optimization
    for (int it = 0; it < (spec.params->RE.alpha_its); ++it)
    {
        tag->dEnorm = searchDirectionPR( spec, spec.dE0->values_d, spec.dE1->values_d, spec.D->values_d, tag);
        tag->step = lineSearchCG(spec.E_h, spec, spec.dE1->values_d, spec.D->values_d, tag, tag->j);
        tag->j+=1;
    }
    spec.params->RE.alpha = tag->step;
    j += spec.params->RE.alpha_its;
}

void updateProbe(Specimen const &spec, int& j)
{
    std::cout<<"\n ________________ Probe Update ________________ \n" << std::endl;
    probe* tag = new probe;
    tag->reset(spec);
    //Determine probe derivative
    spec.E_h[tag->j] = objectiveFunctionSummed<probe>(spec, tag->j);
    //Do conjugate gradient optimization
    for (int it = 0; it < (spec.params->RE.beta_its); ++it)
    {
        tag->dEnorm = searchDirectionPR(spec, spec.dpsi0->values_d, spec.dpsi1->values_d, spec.Dpsi->values_d, tag);
        tag->step = lineSearchCG(spec.E_h, spec, spec.dpsi1->values_d, spec.Dpsi->values_d, tag, tag->j);
        tag->j+=1;
    }

    spec.params->RE.beta = tag->step;
    j += spec.params->RE.beta_its;
}

void updatePositions(Specimen const &spec, int& j)
{
    std::cout<<"\n ________________ Position Update ________________ \n" << std::endl;
    position* tag = new position;
    tag->reset(spec);
    //Determine position derivative
    spec.E_h[tag->j] = objectiveFunctionSummed<position>(spec, tag->j);
    //Do conjugate gradient optimization
    for (int it = 0; it < (spec.params->RE.gamma_its); ++it)
    {
        tag->dEnorm = searchDirectionPR(spec, spec.dpos0, spec.dpos1, spec.Dpos, tag);
        tag->step = lineSearchCG(spec.E_h, spec, spec.dpos1, spec.Dpos, tag, tag->j);
        tag->j+=1;
    }

    spec.params->RE.gamma = tag->step;
    j += spec.params->RE.gamma_its;
}

void applyEqualSlice(Array* dV, params_t* params, params_t* params_d)
{
    cufftComplex* temp;
    cuda_assert(cudaMalloc((void**)&temp, params->IM.ObjectDim * params->IM.ObjectDim * sizeof(cufftComplex)));
    sumValues<<< dV->gS, dV->bS >>>(temp, dV->values_d, params_d);
    cuda_assert(cudaFree(temp));
}

template <typename T>
float objectiveFunctionSummed(Specimen const& spec, int j)
{
    //create temporary arrays
    Array* tempV = new Array(spec.params->IM.ProbeDim, spec.params->IM.ProbeDim, spec.params->IM.Slices, spec.params->IM.batchSize, spec.params->CU);
    Array* tempDE = new Array(spec.params->IM.ProbeDim, spec.params->IM.ProbeDim, (spec.params->IM.Slices + 3), spec.params->IM.batchSize, spec.params->CU);
    Array* tempDpsi = new Array(spec.params->IM.ProbeDim, spec.params->IM.ProbeDim, 1, spec.params->IM.batchSize, spec.params->CU); //*(spec.Dpsi)
    float* tempDpos1 = new float[spec.params->SCN.positions];
    //set derivatives to 0
    spec.dE1->initialize();
    spec.dpsi1->initialize();
    //set error to 0 
    float Err = 0.f;
    //specify batches
    const int pos = spec.params->SCN.BeamPos.size()/2;
    int batchTotal = pos / spec.params->IM.batchSize;

    for (int batchI = 0; batchI < batchTotal; batchI++)
    {
        //clear tempDpsi
        tempDpsi->initialize();
        //clear tempDE
        tempDE->initialize();
        //split potential into sub-potentials and translate
        shift(tempV, spec.V, batchI, spec.params_d, spec.params->SCN.BeamPosRaw, 1);
        //forward propagation
        forwardPropagation ( spec.psi, tempV, spec.params, spec.params_d);
        //error Metric
        Err += errorFunction(tempDE, &(spec.I_d[batchI * spec.params->IM.batchSize * spec.params->IM.CBEDDim * spec.params->IM.CBEDDim ]), spec.psi, spec.params, spec.params_d);
        //backward propagation
	backwardPropagation(tempDE, spec.psi, tempDpsi, tempV, spec.params, spec.params_d);
	//obtain gradients
        derivativeFunction(spec, tempDE, tempV, tempDpsi, tempDpos1, batchI, typename T::classmode{});
    }

    //set slices equal if specified
    if( spec.params->IM.equalSlice == 1)
    {
        applyEqualSlice(spec.dE1, spec.params, spec.params_d);
    }
    //apply regularization
    spec.L1_h[j] = regularizationFunction(spec.dE1->values_d, spec.V->values_d, spec.params, spec.params_d);

    delete tempDpos1;
    delete tempV;
    delete tempDE;
    delete tempDpsi;

    return( Err + spec.L1_h[j]);
}

void derivativeFunction (Specimen const& spec, Array* dEdV, Array* V, Array* dpsi_t, float* dpos_t, int batch, obj)
{
        // Derivative of E wrt potential
        const float alpha = -2.f;
        const int m123bp3 = spec.params->IM.ProbeDim * spec.params->IM.ProbeDim * (spec.params->IM.Slices+3) * spec.params->IM.batchSize;
        cublas_assert ( cublasCsscal ( spec.params->CU.cublasHandle, m123bp3, &alpha, ( cuComplex* ) dEdV->values_d, 1 ) ); //needs to be fixed for probe update (m123p3b -> m123)
        cuda_assert ( cudaDeviceSynchronize () );
        //swap real and imaginary part
        cublas_assert( cublasSswap ( spec.params->CU.cublasHandle, m123bp3, & ( ( ( float* ) dEdV->values_d )[0] ), 2, & ( ( ( float* ) dEdV->values_d )[1] ), 2 ) );
        cuda_assert ( cudaDeviceSynchronize () );
        //merge the derivative dEdV
        shift(dEdV, spec.dE1, batch, spec.params_d, spec.params->SCN.BeamPosRaw, -1);
}

void derivativeFunction (Specimen const& spec, Array* dEdV, Array* V, Array* dpsi_t, float* dpos_t, int batch, prb)
{
    // Derivative of E wrt psi
    const float beta = 2.f;
    batchedCaxpy<<<dpsi_t->gS, dpsi_t->bS>>> (spec.dpsi1->values_d, dpsi_t->values_d, beta, spec.params_d, 0, 1);
    cuda_assert ( cudaDeviceSynchronize () );
}

void derivativeFunction (Specimen const& spec, Array* dEdV, Array* V, Array* dpsi_t, float* dpos_t, int batch, pos)
{
    // Derivative of E wrt positions
    updateProbePositions(spec, dpsi_t, dpos_t, batch);
    cuda_assert(cudaMemcpy(spec.dpos1, dpos_t, spec.params->SCN.positions * sizeof(float), cudaMemcpyHostToDevice));
}


float errorFunction (Array* dE, float* I, Array* psi, params_t* params, params_t* params_d )
{
    float E = 0.f;
    int errorFunctionType = params->RE.ErrorFunction;

    if ( errorFunctionType == 1 )
    {    E = errorFunction1 ( dE, I, psi, params, params_d ); }
    if ( errorFunctionType == 2 )
    {    E = errorFunction2 ( dE, I, psi, params, params_d ); }
    if ( errorFunctionType == 3 )
    {    E = errorFunction3 ( dE, I, psi, params, params_d ); }

	return( E );
}

float errorFunction1 ( Array* dE, float* I, Array* psi, params_t* params, params_t* params_d )
{
    // Absolute values of the differences
    cublasHandle_t cublasHandle = params->CU.cublasHandle;
    const int n12b  = params->IM.CBEDDim * params->IM.CBEDDim * params->IM.batchSize;
    double E;
    float alpha = -1.f;
    int gS = n12b / params->CU.bS + 1;

    float* Imodel;
    double *ImodelD;

    cuda_assert ( cudaMalloc ( ( void** ) &Imodel,  n12b * sizeof ( float ) ) );
    cuda_assert ( cudaMalloc ( ( void** ) &ImodelD,  n12b * sizeof ( double ) ) );

    copyMiddleOut<<< psi->gS, psi->bS>>>  ( Imodel, psi->values_d, params_d );

    cublas_assert ( cublasSaxpy ( cublasHandle, n12b, &alpha , I, 1, Imodel, 1 ) );
    copySingle2Double <<< gS , params->CU.bS >>> ( ImodelD, Imodel, n12b );
    cuda_assert ( cudaDeviceSynchronize () );

    cublas_assert ( cublasDasum ( cublasHandle, n12b, ImodelD, 1, &E ) );
    cuda_assert ( cudaDeviceSynchronize () );

    //Do derivatives
    signValues <<< gS, params->CU.bS >>> ( Imodel, n12b );
    cuda_assert ( cudaDeviceSynchronize () );
    copyMiddleIn<<< dE->gS, dE->bS>>>  ( dE->values_d, Imodel, params_d );
    cuda_assert ( cudaDeviceSynchronize () );

    cuda_assert ( cudaFree ( Imodel ) );
    cuda_assert ( cudaFree ( ImodelD ) );

    return ( ( (float) E ) );
}

float errorFunction2 ( Array* dE, float* I, Array* psi, params_t* params, params_t* params_d)
{
    // Sum of squared differences
    cublasHandle_t cublasHandle = params->CU.cublasHandle;
    const int n12b  = params->IM.CBEDDim * params->IM.CBEDDim * params->IM.batchSize;
    float E;
    float alpha = -1.f;

    float* Imodel;
    cuda_assert ( cudaMalloc ( ( void** ) &Imodel,  n12b * sizeof ( float ) ) );

    copyMiddleOut<<< psi->gS, psi->bS>>>  ( Imodel, psi->values_d, params_d );
    cuda_assert ( cudaDeviceSynchronize () );
    cublas_assert ( cublasSaxpy ( cublasHandle, n12b, &alpha , I, 1, Imodel, 1 ) );
    cuda_assert ( cudaDeviceSynchronize () );
    cublas_assert ( cublasSnrm2 ( cublasHandle, n12b, Imodel, 1, &E ) );
    cuda_assert ( cudaDeviceSynchronize () );

    E *= 0.5f * E; // times E because Snrm2 takes the sqrt

    //Do derivatives
    copyMiddleIn<<< dE->gS, dE->bS>>>  ( dE->values_d, Imodel, params_d );
    cuda_assert ( cudaDeviceSynchronize () );

    cuda_assert ( cudaFree ( Imodel ) );

    return ( E );
}

float errorFunction3 ( Array* dE, float* I, Array* psi, params_t* params, params_t* params_d )
{
    // Absolute values of the differences divided by sqrt( I )
    cublasHandle_t cublasHandle = params->CU.cublasHandle;
    const int n12b  = params->IM.CBEDDim * params->IM.CBEDDim * params->IM.batchSize;
    double E;
    float alpha = -1.f;

    float *Imodel, *Itmp, *I_;
    double *ImodelD;

    cuda_assert ( cudaMalloc ( ( void** ) &Imodel,  n12b * sizeof ( float ) ) );
    cuda_assert ( cudaMalloc ( ( void** ) &Itmp,  n12b * sizeof ( float ) ) );
    cuda_assert ( cudaMalloc ( ( void** ) &I_,  n12b * sizeof ( float ) ) );

    cuda_assert ( cudaMalloc ( ( void** ) &ImodelD,  n12b * sizeof ( double ) ) );
    const int bS = params->CU.bS;
    int gS = n12b / bS + 1;

    //Imodel = I
    copyMiddleOut<<< psi->gS, psi->bS>>> ( Imodel, psi->values_d, params_d );
    //I_ = I
    cuda_assert(cudaMemcpy(I_, Imodel, n12b * sizeof(float), cudaMemcpyDeviceToDevice));
    //Itmp = J
    cuda_assert(cudaMemcpy(Itmp, I, n12b * sizeof(float), cudaMemcpyDeviceToDevice));
    //Itmp = J * log( I + epsilon)
    logValues <<< gS, bS >>> ( Itmp, Imodel, n12b );
    //I_ = I + epsilon
    addEpsilon <<< gS, bS >>> ( I_, n12b );
    // I_ = I + epsilon - J ln( I+ epsilon)
    cublas_assert ( cublasSaxpy ( cublasHandle, n12b, &alpha , Itmp, 1, I_, 1 ) );
    cuda_assert ( cudaDeviceSynchronize () );

    copySingle2Double <<< gS , bS >>> ( ImodelD, I_, n12b );
    cuda_assert ( cudaDeviceSynchronize () );
    cublas_assert ( cublasDasum ( cublasHandle, n12b, ImodelD, 1, &E ) );
    cuda_assert ( cudaDeviceSynchronize () );

    //Do derivatives
    //I_ = J
    cuda_assert(cudaMemcpy(I_, I, n12b * sizeof(float), cudaMemcpyDeviceToDevice));
    //I_ = J / (I + epsilon)
    divideBy <<< gS, bS >>> ( I_, Imodel, n12b );
    cuda_assert ( cudaDeviceSynchronize () );
    //Itmp = 1
    initialValuesFloat <<< gS, bS >>> ( Itmp, n12b , 1.f);
    //Imodel = 1 - J / (I + epsilon)
    cublas_assert ( cublasSaxpy ( cublasHandle, n12b, &alpha , I_, 1, Itmp, 1 ) );
    cuda_assert ( cudaDeviceSynchronize () );

    copyMiddleIn<<< dE->gS, dE->bS>>> ( dE->values_d, Itmp, params_d );
    cuda_assert ( cudaDeviceSynchronize () );

    cuda_assert ( cudaFree ( Imodel ) );
    cuda_assert ( cudaFree ( Itmp ) );
    cuda_assert ( cudaFree ( I_ ) );

    cuda_assert ( cudaFree ( ImodelD ) );

    return ( ( (float) E ) );
}


float regularizationFunction ( cuComplex* dEdV, cuComplex* V, params_t* params, params_t* params_d )
{
    const int l1 = params->IM.ObjectDim;
    const int l2 = params->IM.ObjectDim;
    const int m3 = params->IM.Slices;
    const int bS = params->CU.bS;
    float fctr = ( ( float ) ( params->IM.CBEDDim * params->IM.CBEDDim ) ) / ( ( float ) ( l1 * l2 * m3 ) ) * params->RE.mu;
    float asum = 0.f;

    int l123 = params->IM.ObjectDim * params->IM.ObjectDim * params->IM.Slices;
    float norm = 1.f/sqrtf((float)l123);
    int gS3 = (l123 % 1024 == 0) ? (l123 / 1024) : (l123 / 1024 + 1);


    if( fabsf( params->RE.mu ) > FLT_EPSILON  )
    {
        if ( params->RE.maskFlag == 0)
        {
	    // l1-regularization of V
            asum = sumAbsValues( V, l1 * l2 * m3, params->RE.negAbsFctr );
            // Derivative of regularization term
            addDerivativeAbsValues <<< 2*gS3, bS >>> ( dEdV, V, fctr, l1 * l2 * m3, params->RE.negAbsFctr );
            cuda_assert ( cudaDeviceSynchronize () );
        }
        if ( params->RE.maskFlag == 1)
        {

	    Array *Vk = new Array(params->IM.ObjectDim, params->IM.ObjectDim, params->IM.Slices, 1, params->CU);
	    cufftComplex* Vkx;
            cuda_assert ( cudaMalloc ( ( void** ) &Vkx, params->IM.ObjectDim * params->IM.ObjectDim * params->IM.Slices * sizeof ( cufftComplex ) ) );
	    cufftComplex* Vky;
            cuda_assert ( cudaMalloc ( ( void** ) &Vky, params->IM.ObjectDim * params->IM.ObjectDim * params->IM.Slices * sizeof ( cufftComplex ) ) );
	    float* W;
            cuda_assert ( cudaMalloc ( ( void** ) &W, params->IM.ObjectDim * params->IM.ObjectDim * params->IM.Slices * sizeof ( float ) ) );

	    if (params->RE.sphere_radius != 0.f)
	    {missingSphere<<<Vk->gS, Vk->bS>>>(W, params_d);}
	    else if (params->RE.gauss_sigma != 0.f)
	    {missingGauss<<<Vk->gS, Vk->bS>>>(W, params_d);}
	    else
	    {missingWedge<<<Vk->gS, Vk->bS>>>(W, params_d);}
            cuda_assert(cudaDeviceSynchronize());

            copyObjectIn <<< Vk->gS, Vk->bS >>> ( Vkx, Vky, V, params_d);
            cuda_assert(cudaDeviceSynchronize());
            cufft_assert(cufftExecC2C(params->CU.cufftPlan3D, Vkx, Vkx, CUFFT_FORWARD ) ); 
	    cufft_assert(cufftExecC2C(params->CU.cufftPlan3D, Vky, Vky, CUFFT_FORWARD ) );
            scaleObject <<< Vk->gS, Vk->bS >>> ( Vkx, norm, params_d);
            scaleObject <<< Vk->gS, Vk->bS >>> ( Vky, norm, params_d);
	    copyObjectOut <<<Vk->gS, Vk->bS >>> (Vk->values_d, Vkx, Vky, params_d);
            cuda_assert(cudaDeviceSynchronize());

            // Missing wedge - regularization of V
            asum = sumWedgeAbsValues( Vk->values_d, W, l1 * l2 * m3);
            createWedgeDerivative<<< Vk->gS, Vk->bS >>> (Vkx, Vky, W, params_d);
            cuda_assert ( cudaDeviceSynchronize () );

            cufft_assert(cufftExecC2C(params->CU.cufftPlan3D, Vkx, Vkx, CUFFT_FORWARD ) );
            cufft_assert(cufftExecC2C(params->CU.cufftPlan3D, Vky, Vky, CUFFT_FORWARD ) );
            scaleObject <<< Vk->gS, Vk->bS >>> ( Vkx, norm, params_d);
            scaleObject <<< Vk->gS, Vk->bS >>> ( Vky, norm, params_d);

            addObjectOut <<<Vk->gS, Vk->bS >>> (dEdV, Vkx, Vky, fctr, params_d);
            cuda_assert ( cudaDeviceSynchronize () );

	    cuda_assert(cudaFree(W));
	    cuda_assert(cudaFree(Vkx));
	    cuda_assert(cudaFree(Vky));
        }
    }
    return ( fctr * asum );
}


__global__ void missingWedge(float* W, params_t* params_d)
{
        int i1 = threadIdx.z + blockDim.z *blockIdx.z;
        int i2 = threadIdx.y + blockDim.y *blockIdx.y;
        int i;
	float k1, k2, k3;
	const float pi = params_d->cst.pi;
	int dimr = params_d->IM.ObjectDim;
	int dimz = params_d->IM.Slices;
	float dr = params_d->IM.PixelSize;
	float dz = params_d->IM.SliceDistance;
	float zr_weighting = params_d->RE.beta_wedge;

        iwCoord(k1, i1, dimr);
        k1 /= float(dimr * dr);
        iwCoord(k2, i2, dimr);
        k2 /= float(dimr * dr);

        float kr = sqrtf( powf(k1, 2.f) + powf(k2, 2.f) );
	kr *= dr;

        for (int i3 = 0; i3 < dimz; i3 += 1)
	{
            sgCoord3D(i, i1, i2, i3 , dimr, dimr);
            iwCoord(k3, i3, dimz);
	    k3 *= zr_weighting / float(dimz * dz);
	    k3 *= dr;
            float w = atan2f(powf(k3, 2.f) , powf(kr, 2.f) ) / (pi / 2.f);

            W[i] = w;
	}
}


__global__ void missingSphere(float* S, params_t* params_d)
{
        int i1 = threadIdx.z + blockDim.z *blockIdx.z;
        int i2 = threadIdx.y + blockDim.y *blockIdx.y;
        int i;
	float s, k, k1, k2, k3;
        int dimr = params_d->IM.ObjectDim;
        int dimz = params_d->IM.Slices;
        float dr = params_d->IM.PixelSize;
        float dz = params_d->IM.SliceDistance;
        float zr_weighting = params_d->RE.beta_wedge;
	float radius = params_d->RE.sphere_radius;

        iwCoord(k1, i1, dimr);
        k1 /= float(dimr * dr);
        iwCoord(k2, i2, dimr);
        k2 /= float(dimr * dr);

        for (int i3 = 0; i3 < dimz; i3 += 1)
        {
            sgCoord3D(i, i1, i2, i3 , dimr, dimr);
            iwCoord(k3, i3, dimz);
	    k3 *= (zr_weighting / float(dimz * dz));
            k = sqrtf( powf(k1, 2.f) + powf(k2, 2.f) + powf(k3, 2.f));
	    k *= dr;
            if (k < (radius/dimr))
            {
                s = 0.f;
            }
            else
            {
                s = 1.f;
            }

            S[i] = s;
        }
}


__global__ void missingGauss(float* G, params_t* params_d)
{
        int i1 = threadIdx.z + blockDim.z *blockIdx.z;
        int i2 = threadIdx.y + blockDim.y *blockIdx.y;
        int i;
	float k, k1, k2, k3;
        int dimr = params_d->IM.ObjectDim;
        int dimz = params_d->IM.Slices;
        float dr = params_d->IM.PixelSize;
        float dz = params_d->IM.SliceDistance;
        float zr_weighting = params_d->RE.beta_wedge;
	float sigma = params_d->RE.gauss_sigma;

        iwCoord(k1, i1, dimr);
        k1 /= float(dimr * dr);
        iwCoord(k2, i2, dimr);
        k2 /= float(dimr * dr);

        for (int i3 = 0; i3 < dimz; i3 += 1)
        {
            sgCoord3D(i, i1, i2, i3 , dimr, dimr);
            iwCoord(k3, i3, dimz);
	    k3 *= zr_weighting / float(dimz * dz);
            k = sqrtf( powf(k1, 2.f) + powf(k2, 2.f) + powf(k3, 2.f));
	    k *= dr;
            float g = 1.f - expf(-powf(k, 2.f) / ( 2 * powf(sigma, 2.f) ) );

            G[i] = g;
        }
}



template <typename T>
float lineSearchCG ( float* E_h, Specimen const &spec, typename T::typemode* dE1_d, typename T::typemode* const D_d, T* tag, int j )
{
    // A line search based on the secant method to find the step size for CONJUGATE GRADIENTS
    // ATTENTION: This search ends with the values of E_h, V, dE_D and dNuis changed to those for the NEXT iteration 
    int iterMax = 10, flag = 0, cnt;
    float dO, d0, d1, d2, alpha0, alpha1, alpha2, varTemp1, varTemp2, E0, EO, E1, E2, c1, c2, alphaHi;

    // Set parameters for the strong Wolfe conditions
    c1 = spec.params->RE.cgC1;
    c2 = spec.params->RE.cgC2;

    // Safety bracket for the step size (is expanded in a controlled way as needed)
    tag->initStep[0] = fabsf( tag->initStep[0] ); // Redundant safety measure, alphaInit should always be strictly positive
    alphaHi = 2.f * tag->initStep[0];

    // Calculate the derivative along the search direction D_d
    tag->dot(spec, dE1_d, D_d, dO);
    dO *= -1.f;

    if ( j == 0 )
    {
	    alpha1 = tag->initStep[0];
    }
    else
    {
	    alpha1 = 2.f * ( E_h[j] - E_h[j-1] ) / dO;

	    if ( alpha1 < 1e-17f )
	    {
		    alpha1 = 1e-17f;
	    }
	    if ( alpha1 > alphaHi )
	    {
		    alpha1 = alphaHi;
	    }
    }

    alpha0 = 0.f;
    E0 = E_h[j];
    d0 = dO;
    EO=E0;

    ////First test if strong Wolfe condition is fullfilled immediatly
    // Get value of the error function at V + alpha*D_d
    takeStepCG (spec, D_d, alpha1, tag);
    E1 = objectiveFunctionSummed<T>(spec, j);
    takeStepCG (spec, D_d, -alpha1, tag); // Go back to start, is slower, but only negligibly so.

    // Calculate the derivative along the search direction D_d
    tag->dot(spec, D_d, dE1_d, d1);
    d1 *= -1.f;

    cnt = 0;
    for (int i=1; i < 100; i++)
    {
        cnt++;
        std::cout <<"Current Error: "<< EO << "; Error at step: " << E1 << "; Current Derivative: " << dO << "; Derivative at step: "<< d1 <<"; step size: "<< alpha1 <<"\n" << std::flush;
        if ( ( E1 < ( E_h[j] + c1 * alpha1 * dO ) ) && ( fabs( d1 ) < c2 * fabs( dO ) ) )
        {break;}

        if(d1>0)
        {break; }
        alpha1 = alpha1 * 2.f;

        takeStepCG ( spec, D_d, alpha1, tag);
        varTemp1 = E1;
        E1 = objectiveFunctionSummed<T>(spec, j);
        takeStepCG ( spec, D_d, -alpha1, tag); // Go back to start, is slower, but only negligibly so.
        // Calculate the derivative along the search direction D_d
        varTemp2 = d1;
        tag->dot(spec, D_d, dE1_d, d1);
        d1 *= -1.f;
    }

    alpha2 = alpha1;
    E2=E1;
    d2=d1;

    if ( cnt > 1 )
    {
        alpha0 = alpha1 * 0.5f;
        E0 = varTemp1;
        d0 = varTemp2;
    }

    for ( int i = 0; i < iterMax; i++ )
    {
        // First ten times, shoot for the strong wolfe conditions
        if( i < 100 )
        {
            // Test for strong Wolfe conditions
            if ( ( E2 < ( E_h[j] + c1 * alpha2 * dO ) ) && ( fabs( d2 ) < c2 * fabs( dO ) ) )
	    {
    	        flag = 1;
                E1=E2;
	        alpha1=alpha2;
	        break;
	    }
	    
        // Enforce alpha0 < alpha1
        varTemp1 = fminf( alpha0, alpha1 );
        alpha1 = fmaxf( alpha0, alpha1 );
        alpha0 = varTemp1;

        // Find the position of the minimum of the cubic function fitted to E0, d0, E1 and d1:
        if ( ( i > 10 ) || ( i == 5 ) || ( j == 0 ) )
        {
	    c2 = powf( c2, 0.71 );
	    alpha2 = 0.5f * ( alpha0 + alpha1 );
        }
        else
        {
	    alpha2 = minimumThirdOrderPoly(alpha0, alpha1, E0, E1, d0, d1);
	    // Fail-safe to prevent too-high extrapolation
	    alpha2 = fmaxf(alpha2, alpha0 * 0.5f);
	    alpha2 = fminf(alpha2, alpha1 * 2.0f);
        }

        // Get value of the error function at V + alpha*D_d
        takeStepCG ( spec, D_d, alpha2, tag);
        E2 = objectiveFunctionSummed<T>(spec, j);
        takeStepCG ( spec, D_d, -alpha2, tag); // Go back to start, is slower, but only negligibly so.

        // Calculate the derivative along the search direction D_d
        tag->dot(spec, D_d, dE1_d, d2);
        d2 *= -1.f;

        std::cout <<"Current Error: "<< EO << "; Error at step: " << E2 << "; rel. Derivative: " << d2/d0 << "; step size: "<< alpha2 <<"\n" << std::flush;

        if(d2<0)
        {
	    E0=E2;
	    d0=d2;
	    alpha0=alpha2;
        }

        if(d2>0)
        {
	    E1=E2;
	    d1=d2;
	    alpha1=alpha2;
        }

        // Stop if alpha1 is too small
        if ( alpha2 < 1e-17f )
        {
            flag = 3;
            alpha1 = 0.f;
            fprintf ( stderr, "   Stopping optimization because stepsize is below 1e-9, setting stepsize to 0.\n" );
            break;
        }
    }

    // Now try the weak wolfe conditions
    else
    {
	if ( E2 < ( E_h[j] + c1 * alpha2 * d2 )  )
        {
            flag = 2;
	    alpha1 = alpha2;
	    fprintf ( stderr, "  Stopping line search because weak Wolfe conditions are satisfied.\n" );
	    break;
        }

        alpha2 *= 0.31622776f; // Decrease step size with factor of sqrt(0.1) until weak wolfe condition is met.
        // Stop if alpha0 is too small
        if ( alpha2 < 1e-17f )
        {
            flag = 3;
	    alpha1 = 0.f;
	    fprintf ( stderr, "   Stopping optimization because stepsize is below 1e-9, setting stepsize to 0.\n" );
	    break;
        }
    }
    }

    if ( flag == 0 ) // Means neither Weak nor Strong Wolfe conditions were met. The whole optimization is stopped, because something must be wrong.
    {
        flag = 3;
	alpha1 = 0.f;
	fprintf ( stderr, "  Stopping optimization because line search has reached maximum of %i iterations. Setting stepsize to 0.\n", iterMax-1 );
    }

    takeStepCG ( spec, D_d, alpha1, tag);

    if ( flag == 3 )// Step size is 0, so set error function equal to previous one. Now the derivatives (dE_D and dNuis) don't match anymore, but that's ok, the program terminates anyway before they'd be used.
    {   E1 = E_h[j]; }
    if ( flag == 2 ) // Terminated with weak Wolfe conditions. Re-evaluate objective function so that derivatives (dE_D and dNuis) match the current step size
    {   E1 = objectiveFunctionSummed<T>(spec, j);}

    E_h[j+1] = E1;

    return( alpha1 );
}

void takeStepCG ( Specimen const &spec, cufftComplex* dEdV, float alpha, object* tag)
{
	const int l123 = spec.params->IM.ObjectDim * spec.params->IM.ObjectDim * spec.params->IM.Slices;
	const float step = -alpha; // Minus because we step down the search direction. This is OPPOSITE of common practice! (for legacy reasons, sorry)

	cublas_assert ( cublasSaxpy ( spec.params->CU.cublasHandle, 2 * l123, &step, (float*) dEdV, 1, (float*) spec.V->values_d, 1 ) );
	cuda_assert ( cudaDeviceSynchronize () );
}

void takeStepCG ( Specimen const &spec, cufftComplex* dpsi, float beta, probe* tag)
{
    const int m12 = spec.params->IM.ProbeDim * spec.params->IM.ProbeDim;
    const float step = -beta; // Minus because we step down the search direction. This is OPPOSITE of common practice! (for legacy reasons, sorry)

    cufftComplex *dpsi_t;
    cuda_assert ( cudaMalloc ( ( void** ) &dpsi_t, m12 * sizeof ( cufftComplex ) ) );
    cuda_assert ( cudaMemcpy (dpsi_t, dpsi, m12 * sizeof ( cufftComplex ), cudaMemcpyDeviceToDevice ) );

    cublas_assert ( cublasCsscal ( spec.params->CU.cublasHandle, spec.params->IM.ProbeDim * spec.params->IM.ProbeDim, &step, dpsi_t, 1 ) );
    addIn<<< spec.psi->gS, spec.psi->bS >>>(spec.psi->values_d, dpsi_t, spec.params_d, 0);

    cuda_assert(cudaFree(dpsi_t));
}

void takeStepCG( Specimen const &spec, float* dpos, float gamma, position* tag)
{
    const int size = spec.params->SCN.positions;
    const float step = gamma;
    const float stepSize = spec.params->SCN.ScanDistanceX;
    const float invStepSize = 1/spec.params->SCN.ScanDistanceX;

    cublas_assert(cublasSscal(spec.params->CU.cublasHandle, spec.params->SCN.positions, &stepSize, dpos, 1));
    cublas_assert ( cublasSaxpy ( spec.params->CU.cublasHandle, size, &step, dpos, 1, spec.params->SCN.BeamPosRaw, 1 ) );
    cublas_assert(cublasSscal(spec.params->CU.cublasHandle, spec.params->SCN.positions, &invStepSize, dpos, 1));
    cuda_assert ( cudaDeviceSynchronize () );
}

float minimumThirdOrderPoly( float x0, float x1, float f0, float f1, float dF0, float dF1 )
{
	float a = 0.f, b = 0.f, c = 0.f;
	float dF1x01 = 0.f;
	float Det = 0.f;
	float xMin = 0.f;

	// Find the coefficients of the polynomial a * x^3 + b * x^2 + c * x + d = f
	// but d is not needed
	c      = dF0 * ( x1 - x0 );
	dF1x01 = dF1 * ( x1 - x0 );
	a =  2.f * f0 - 2.f * f1 +       c + dF1x01;
	b = -3.f * f0 + 3.f * f1 - 2.f * c - dF1x01;

	// Now determine the position of the minimum
	Det = b * b - 3.f * a * c;
	if ( Det > 0.f )
	{
		Det = sqrtf( Det );
		xMin = ( -b + Det ) / ( 3.f * a );
		if( ( 6.f * a * xMin + 2.f * b ) < 0.f )
		{    xMin = ( -b - Det ) / ( 3.f * a ); }
		xMin *= ( x1 - x0 );
		xMin += x0;
	}
	else { if ( ( ( f1 - f0 ) * ( x1 - x0 ) ) > 0.f ) // Should be a division "/", but only the sign is needed, so a multiplication gives that as well, but without the risk of dividing by zero
	{    xMin = -FLT_MAX * 0.1f; }
	else
	{    xMin =  FLT_MAX * 0.1f; } }

	return( xMin );

	// NOTE at the end. This is the expression for a quadratic function:
	// x1 = x1 - x0;
	// x1 =  x0 + 0.5f * dF0 * x1 * x1 / ( f0 - f1 + x1 * dF0 + 1e-8f );
	// This does not use dF1, only the value and the derivative in x0 and the value in x1
}

float minimumSecondOrderPoly( float x0, float x1, float f0, float f1, float dF0)
{
    x1 = x1 - x0;
    x1 =  x0 + 0.5f * dF0 * x1 * x1 / ( f0 - f1 + x1 * dF0 + 1e-8f );
    return x1;
}

void saveResults ( cufftComplex* V, cufftComplex* psi, float* E, float* L1, int j, params_t* params)
{
    //Save object
    std::string sr = "PotentialReal" + std::to_string(j) +".bin";
    char * cr = new char[sr.size() + 1];
    std::strcpy(cr, sr.c_str());

    std::string si =  "PotentialImag" + std::to_string(j) +".bin";
    char * ci = new char[si.size() + 1];
    std::strcpy(ci, si.c_str());

    const int l123 = params->IM.ObjectDim * params->IM.ObjectDim * params->IM.Slices;
    cufftComplex* V_h;
    V_h = ( cufftComplex* ) malloc ( l123 * sizeof ( cufftComplex ) );
    cuda_assert ( cudaMemcpy ( V_h, V, l123 * sizeof ( cufftComplex ), cudaMemcpyDeviceToHost ) );
    float* Vri;
    Vri = ( float* ) calloc ( l123, sizeof ( float ) );
    realPart ( Vri, V_h, l123 );
    writeBinary ( cr, Vri, l123 );
    imagPart ( Vri, V_h, l123 );
    writeBinary ( ci, Vri, l123 );

    //Save probe
    sr =  "ProbeReal" + std::to_string(j) +".bin";
    std::strcpy(cr, sr.c_str());
    si =  "ProbeImag" + std::to_string(j) +".bin";
    std::strcpy(ci, si.c_str());

    const int m123 = params->IM.ProbeDim * params->IM.ProbeDim * params->IM.Slices;
    cufftComplex* psi_h;
    psi_h = ( cufftComplex* ) malloc ( m123 * sizeof ( cufftComplex ) );
    cuda_assert ( cudaMemcpy ( psi_h, psi, m123 * sizeof ( cufftComplex ), cudaMemcpyDeviceToHost ) );
    float* psiri;
    psiri = ( float* ) calloc ( m123, sizeof ( float ) );
    realPart ( psiri, psi_h, m123 );
    writeBinary ( cr, psiri, m123 );
    imagPart ( psiri, psi_h, m123 );
    writeBinary ( ci, psiri, m123 );

    //Save positions
    float* bpos;
    bpos = (float*)malloc(params->SCN.positions * sizeof(float));
    std::string name = "Positions" + std::to_string(j) +".txt";
    cuda_assert(cudaMemcpy(bpos, params->SCN.BeamPosRaw, params->SCN.positions * sizeof(float), cudaMemcpyDeviceToHost));
    std::ofstream pa(name, std::ios::trunc);
    for (int pos = 0; pos < (params->SCN.positions / 2); ++pos)
    {
        pa << "beam_position: " << "\t" << bpos[pos * 2] << "\t " << bpos[pos * 2 + 1] << std::endl;
        pa.flush();
    }

    writeBinary ((char *)"ErrorFunction.bin", E, params->RE.its );
    writeBinary ((char *) "L1Norm.bin", L1, params->RE.its );
    //writeConfig ( "ParamsUsed.cnf", params );

    delete[] cr;
    delete[] ci;
    free ( Vri );
    free ( V_h );
    free ( psiri );
    free ( psi_h );
}

void saveModelMeasurements(Array* V, Array* psi, params_t* params, params_t* params_d, int j)
{
    std::string sr = "Measurements_model.bin";
    char * cr = new char[sr.size() + 1];
    std::strcpy(cr, sr.c_str());

    const int pos = params->SCN.BeamPos.size()/2;

    float *Imodel_d, *Imodel_h;
    Imodel_h = (float*)calloc(params->IM.CBEDDim * params->IM.CBEDDim * pos, sizeof(float));
    cuda_assert(cudaMalloc((void**)&Imodel_d, params->IM.CBEDDim * params->IM.CBEDDim * params->IM.batchSize * sizeof(float)));

    writeBinary(cr, Imodel_h, 0); // A dummy-write of length 0 to initialize the file

    Array* tempV = new Array(params->IM.ProbeDim, params->IM.ProbeDim, params->IM.Slices, params->IM.batchSize, params->CU);

    int batchTotal = pos / params->IM.batchSize;

    for (int batchI = 0; batchI < batchTotal; batchI++)
    {
        shift(tempV, V, batchI, params_d, params->SCN.BeamPosRaw, 1);
        forwardPropagation ( psi, tempV, params, params_d );
        copyMiddleOut<<< psi->gS, psi->bS>>> ( Imodel_d, psi->values_d, params_d );
        cuda_assert(cudaMemcpy(&(Imodel_h[batchI * params->IM.batchSize * params->IM.CBEDDim * params->IM.CBEDDim ]), Imodel_d, params->IM.CBEDDim * params->IM.CBEDDim * params->IM.batchSize * sizeof(float), cudaMemcpyDeviceToHost));
        appendBinary(cr, &(Imodel_h[batchI * params->IM.batchSize * params->IM.CBEDDim * params->IM.CBEDDim ]), params->IM.CBEDDim * params->IM.CBEDDim * params->IM.batchSize);
    }

	delete tempV;
	cuda_assert(cudaFree(Imodel_d));
	free(Imodel_h);
}

template <typename T>
float searchDirectionPR( Specimen const &spec, typename T::typemode* dE0_d, typename T::typemode* dE1_d, typename T::typemode* D_d, T* tag)
{
    /* Implementation of the Polak-Riberie search direction for conjugate gradients */
    float dEnorm1;
    float dEnorm01;

    // Compute the norm squared of the derivative
    tag->norm(spec, dE1_d, dEnorm1);
    dEnorm1 *= dEnorm1;
    tag->dot(spec, dE0_d, dE1_d, dEnorm01);

    if ( ( abs( dEnorm01 ) / (sqrtf( dEnorm1 * tag->dEnorm ) + FLT_MIN) ) > spec.params->RE.cgprOrthTest )
    {
        tag->gamma = 0.f;
    }
    else
    {
        tag->gamma = ( dEnorm1 - dEnorm01 ) / (tag->dEnorm + FLT_MIN) ;
    }
    if ( tag->gamma < FLT_MIN || tag->gamma > FLT_MAX )
    {
	tag->gamma = 0.f;
    }

    // Construct the next search direction D_d
    tag->scal(spec, D_d);
    tag->axpy(spec, dE1_d, D_d);

    // Copy "current" derivatives and search directions in the "previous"
    tag->copy(spec, dE1_d, dE0_d);
    // Test if D_d is a descent direction
    tag->dot(spec, D_d, dE1_d, tag->gamma);

    if ( ( tag->gamma < FLT_MIN ) || ( tag->step < FLT_MIN ) )
    {
        tag->copy(spec, dE1_d, D_d);
	fprintf ( stderr, "\n  No descent direction, resetting CG.\n" );
    }

    return dEnorm1;
}
