/*------------------------------------------------
	Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
	See ROP.cu for full notice
------------------------------------------------*/

#include "multislice.h"
#include "mathematics.h"
#include "rwBinary.h"

#include <cstdio>
#include "coordArithmetic.h"
#include <cufft.h>
#include "initialization.h"
#include "cuda_assert.hpp"
#include <cuComplex.h>

#include "cufft_assert.h"
#include "cublas_assert.hpp"

#include <fstream>
#include <string>

__global__ void potential2Transmission ( cufftComplex* t, cufftComplex* V, int size )
{
    const int i = blockIdx.x * blockDim.x + threadIdx.x;

    if ( i < size )
    {
        float Vx = V[i].x;
        float Vy = V[i].y;
        t[i].x = expf ( -Vy ) * cosf ( Vx );
        t[i].y = expf ( -Vy ) * sinf ( Vx );
    }
}

__global__ void zeroHighFreq ( cufftComplex* f, int dim1, int dim2 )
{
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    float mindim = ( float ) dim1;
    if ( ( float ) dim2 < mindim )
    { mindim = ( float ) dim2; }

    if ( i < 2 * dim1 * dim2 )
    {
        int i0 = i / 2;
        int i1, i2;
        dbCoord ( i1, i2, i0, dim1 );
        iwCoordIp ( i1, dim1 );
        iwCoordIp ( i2, dim2 );

        if ( ( ( float ) ( i1 * i1 + i2 * i2 ) * 9.f / ( mindim * mindim ) ) > 1.f )
        {
            if ( ( i % 2 ) == 0 )
            { f[i0].x = 0.f; }

            else
            { f[i0].y = 0.f; }
        }
    }
}

__global__ void fresnelPropagatorDevice ( cufftComplex* frProp, params_t* params, int flag )
{
    // flag = 1 for forward propagation and -1 for backward propagation
    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    const int dim1 = params->IM.ProbeDim;
    const int dim2 = params->IM.ProbeDim;

    if ( i < dim1 * dim2 )
    {
        int i1, i2;
        dbCoord ( i1, i2, i, dim1 );
        iwCoordIp ( i1, dim1 );
        iwCoordIp ( i2, dim2 );
        i1 *= flag;
        i2 *= flag;

        float d3 = params->IM.SliceDistance;
        const float t1 = ( ( (float) i1 ) / ( (float) dim1 ) ) * ( d3 / params->IM.PixelSize );
        const float t2 = ( ( (float) i2 ) / ( (float) dim2 ) ) * ( d3 / params->IM.PixelSize );
        d3 = params->EM.lambda / d3;
	// NOTE: From when the specimen rotation was approximate: d3 = params->cst.pi * ( t1 * ( 2.f * tanf ( -params->IM.tiltspec[2 * k + 1] ) - t1 * d3 ) + t2 * ( 2.f * tanf ( -params->IM.tiltspec[2 * k] ) - t2 * d3 ) );
	d3 = -params->cst.pi * d3 * ( t1 * t1 + t2 * t2 );
        frProp[i].x = cosf ( d3 );
        frProp[i].y = sinf ( d3 );
    }
}

__global__ void multiplyLensFunction( cufftComplex* psi, params_t* params, int flag )
{
    // flag = 1 for forwardPropagation and -1 for backwardPropagation

    const int i = blockIdx.x * blockDim.x + threadIdx.x;
    const int dim1 = params->IM.ProbeDim;
    const int dim2 = params->IM.ProbeDim;
    const int batchSize = params->IM.batchSize;

    if ( i < dim1 * dim2 * batchSize )
    {
        int i1, i2;
        dbCoord ( i1, i2, i, dim1);
        iwCoordIp ( i1, dim1 );
        iwCoordIp ( i2, dim2 );
        i1 *= flag;
        i2 *= flag;

        float nu1  = ( ( ( float ) i1 ) / ( ( float ) dim1 ) ) * (params->EM.lambda / params->IM.PixelSize );
        float nu2 = ( ( ( float ) i2 ) / ( ( float ) dim2 ) ) * (params->EM.lambda / params->IM.PixelSize );
        float phi = atan2f ( nu2, nu1 );
        if(i1 == 0)
        {
                if(i2 < 0)
                { phi = -1.570796;}
                else
                { phi = 1.570796;}
        }
        if(i2 == 0)
        {
                if(i1 < 0)
                { phi = 3.141593;}
                else
                { phi = 0.f;}
        }
        float nu = sqrtf ( nu1 * nu1 + nu2 * nu2 );

        if ( nu < params->EM.ObjAp )
        {
            float W = nu * nu * ( 0.5f * ( params->EM.aberration.A1_0 * cosf ( 2.f * ( phi - params->EM.aberration.A1_1 ) )
                    +params->EM.aberration.C1_0 + params->RE.defocPoly0
                    - ((float)(params->IM.Slices + 1)) * params->IM.SliceDistance * 0.5f)
                                  + nu * ( 0.33333333f * (
                    params->EM.aberration.A2_0 * cosf ( 3.f * ( phi - params->EM.aberration.A2_1 ) )
                    + params->EM.aberration.B2_0 * cosf ( phi - params->EM.aberration.B2_1 ) )
                                           + nu * ( 0.25f * (
                    params->EM.aberration.A3_0 * cosf ( 4.f * ( phi - params->EM.aberration.A3_1 ) )
                    + params->EM.aberration.S3_0 * cosf ( 2.f * ( phi - params->EM.aberration.S3_1 ) )
                    + params->EM.aberration.C3_0 )
                                                    + nu * ( 0.2f * (
                    params->EM.aberration.A4_0 * cosf ( 5.f * ( phi - params->EM.aberration.A4_1 ) )
                    + params->EM.aberration.B4_0 * cosf ( phi - params->EM.aberration.B4_1 )
                    + params->EM.aberration.D4_0 * cosf ( 3.f * ( phi - params->EM.aberration.D4_1 ) ) )
                                                             + nu * ( 0.1665f * (
                    params->EM.aberration.A5_0 * cosf ( 6.f * ( phi - params->EM.aberration.A5_1 ) )
                    + params->EM.aberration.R5_0 * cosf ( 4.f * ( phi - params->EM.aberration.R5_1 ) )
                    + params->EM.aberration.S5_0 * cosf ( 2.f * ( phi - params->EM.aberration.S5_1 ) )
                    + params->EM.aberration.C5_0 )
            ) ) ) ) ); // CLOSE ALL THE BRACKETS!

            // Recycle the variables:
            nu = sqrtf ( nu1 * nu1 + nu2 * nu2 );
            nu2 = params->EM.lambda;
            float damp = params->EM.defocspread * nu * nu / nu2;
            damp = expf ( -2.f * damp * damp );
            nu = params->cst.pi;
            phi  = damp * cosf (  2.f * nu * ( W / nu2 ) ); // real part of CTF
            damp = damp * sinf ( -2.f * nu * ( W / nu2 ) ); // imag part of CTF
            nu  = psi[i].x; // real part of psi
            nu2 = psi[i].y; // imag part of psi

            psi[i].x = phi * nu - damp * nu2; // real*real - imag*imag
            psi[i].y = phi * nu2 + damp * nu; // real*imag + imag*real
        }

        else
        {
            psi[i].x = 0.f;
            psi[i].y = 0.f;
        }
    }
}

void shift(Array* fout, Array* fin, int batchI, params_t* params, float* BeamPos_d, int flag){

    switch(flag){
        case 1:
            shiftOut_d<<<fout->gS, fout->bS>>>(fout->values_d, fin->values_d, fout->dimX_h, fin->dimX_h, batchI, params, BeamPos_d);
            break;
        case -1:
            shiftIn_d<<<fout->gS, fout->bS>>>(fout->values_d, fin->values_d, fout->dimX_h, fin->dimX_h, batchI, params, BeamPos_d);
            break;
    }
}


__global__ void shiftOut_d(cufftComplex* fOut, cufftComplex* fIn, int foutdimw, int findimw, int batchI, params_t* params, float* BeamPos_d)
{
    int n1 = threadIdx.z + blockDim.z *blockIdx.z;
    int n2 = threadIdx.y + blockDim.y *blockIdx.y;
    int n3 = params->IM.Slices * blockIdx.x;

    int k, m;
    float x1, x2;
    const int dm = (int) (findimw - foutdimw) / 2;

    float fOutx = 0.f;
    float fOuty = 0.f;

    int xy = batchI * gridDim.x + blockIdx.x;

    x1 = n1 + dm + BeamPos_d[2 * xy] / params->IM.PixelSize;
    x2 = n2 + dm + BeamPos_d[2 * xy + 1] / params->IM.PixelSize;

    int n1_ = roundf(x1 - 0.5f);
    int n2_ = roundf(x2 - 0.5f);

    x1 += -((float) n1_);
    x2 += -((float) n2_);

    for (int slice = 0; slice < params->IM.Slices; slice ++)
    {
        sgCoord3D(m, n1, n2, n3, foutdimw, foutdimw);

        if ((n1_ > -1) && (n1_ < (findimw - 1)) && (n2_ > -1) && (n2_ < (findimw - 1))) {
            sgCoord3D(k, n1_, n2_, slice, findimw, findimw);
            fOutx = fIn[k].x * (1.f - x1) * (1.f - x2);
            fOuty = fIn[k].y * (1.f - x1) * (1.f - x2);

            sgCoord3D(k, n1_ + 1, n2_, slice, findimw, findimw);
            fOutx += fIn[k].x * x1 * (1.f - x2);
            fOuty += fIn[k].y * x1 * (1.f - x2);

            sgCoord3D(k, n1_, n2_ + 1, slice, findimw, findimw);
            fOutx += fIn[k].x * (1.f - x1) * x2;
            fOuty += fIn[k].y * (1.f - x1) * x2;

            sgCoord3D(k, n1_ + 1, n2_ + 1, slice, findimw, findimw);
            fOutx += fIn[k].x * x1 * x2;
            fOuty += fIn[k].y * x1 * x2;
        }
        fOut[m].x = fOutx;
        fOut[m].y = fOuty;

        n3 += 1;
    }
}

__global__ void shiftIn_d(cufftComplex* fOut, cufftComplex* fIn, int foutdimw, int findimw, int batchI, params_t* params, float* BeamPos_d)

{
    int n1 = threadIdx.z + blockDim.z *blockIdx.z;
    int n2 = threadIdx.y + blockDim.y *blockIdx.y;
    int n3 = (params->IM.Slices + 3) * blockIdx.x;

    if ((n2 < (foutdimw - 2))) {

        int n, m;
        float x1, x2;
        const float dm = (float) (findimw - foutdimw) / 2;

        float fInx = 0.f;
        float fIny = 0.f;

        int xy = batchI * gridDim.x + blockIdx.x;

        x1 = n1 + dm + BeamPos_d[2 * xy] / params->IM.PixelSize;
        x2 = n2 + dm + BeamPos_d[2 * xy + 1] / params->IM.PixelSize;

        int n1_ = roundf(x1 - 0.5f);
        int n2_ = roundf(x2 - 0.5f);

        x1 += -((float) n1_);
        x2 += -((float) n2_);

        for (int slice = 0; slice < params->IM.Slices; slice ++)
        {
            sgCoord3D(m, n1, n2, n3, foutdimw, foutdimw);

            if ((n1_ > -1) && (n1_ < (findimw - 1)) && (n2_ > -1) && (n2_ < (findimw - 1)))
            {
                sgCoord3D(n, n1_, n2_, slice, findimw, findimw);
                fInx = fOut[m].x * (1.f - x1) * (1.f - x2);
                fIny = fOut[m].y * (1.f - x1) * (1.f - x2);
                atomicAdd(&(fIn[n].x), fInx);
                atomicAdd(&(fIn[n].y), fIny);

                sgCoord3D(n, n1_ + 1, n2_, slice, findimw, findimw);
                fInx = fOut[m].x * x1 * (1.f - x2);
                fIny = fOut[m].y * x1 * (1.f - x2);
                atomicAdd(&(fIn[n].x), fInx);
                atomicAdd(&(fIn[n].y), fIny);

                sgCoord3D(n, n1_, n2_ + 1, slice, findimw, findimw);
                fInx = fOut[m].x * (1.f - x1) * x2;
                fIny = fOut[m].y * (1.f - x1) * x2;
                atomicAdd(&(fIn[n].x), fInx);
                atomicAdd(&(fIn[n].y), fIny);

                sgCoord3D(n, n1_ + 1, n2_ + 1, slice, findimw, findimw);
                fInx = fOut[m].x * x1 * x2;
                fIny = fOut[m].y * x1 * x2;
                atomicAdd(&(fIn[n].x), fInx);
                atomicAdd(&(fIn[n].y), fIny);
            }
            n3 +=1;
        }
    }
}


void forwardPropagation ( Array* psi, Array* V, params_t* params, params_t* params_d)
{
    const int size_single = params->IM.ProbeDim * params->IM.ProbeDim;
    const int size_batch = params->IM.ProbeDim * params->IM.ProbeDim * params->IM.batchSize;

    // Construct Fresnel propagator
    cufftComplex* frProp;
    cuda_assert ( cudaMalloc ( ( void** ) &frProp, size_single * sizeof ( cufftComplex ) ) );
    fresnelPropagator ( frProp, params, params_d, 1 );

    // Allocate the transmission function
    cufftComplex* t;
    cuda_assert ( cudaMalloc ( ( void** ) &t, size_batch * sizeof ( cufftComplex ) ) );

    // Do the propagation through the specimen
    for ( int z = 0; z < params->IM.Slices; z++ )
    {
	// Copy potential
        copyBatchInOut<<<V->gS, V->bS>>> ( t, V->values_d, params_d, z, 1);
        cuda_assert ( cudaDeviceSynchronize () );
	// Convert potential to transmission function
        potential2Transmission <<< params->CU.gSB, params->CU.bS>>> ( t, t, size_batch ); 
        cuda_assert ( cudaDeviceSynchronize () );
	// Limit bandwidth of transmission function
        bandwidthLimit ( t, params );
	// Interaction between probe and object
        probeObjectInteraction<<< psi->gS, psi->bS>>> ( t, psi->values_d, params_d, z, 1 );
        cuda_assert ( cudaDeviceSynchronize () );
	// Propagation to next slice
        convolveWithFrProp ( t, frProp, params, params_d); 
	// Form exit wave
        copyPsiBatchInOut<<< psi->gS, psi->bS>>>  (psi->values_d, t, params_d, z + 1, -1 );
        cuda_assert ( cudaDeviceSynchronize () );
    }

    // Far field propagation of exit wave
    farFieldTransform(psi, params, params_d);
    // Take the intensity
    doIntensity(psi, params, params_d, (params->IM.Slices + 1));

    cuda_assert ( cudaFree ( frProp ) );
    cuda_assert ( cudaFree ( t ) );
}

void backwardPropagation ( Array* dEdV, Array* psi, Array* dpsi_t, Array* V, params_t* params, params_t* params_d)
{
    const int size_single = params->IM.ProbeDim * params->IM.ProbeDim;
    const int size_batch = params->IM.ProbeDim * params->IM.ProbeDim * params->IM.batchSize;
    int z_batch = 0;

    // Allocate the transmission function
    cufftComplex* t;
    cuda_assert ( cudaMalloc ( ( void** ) &t, size_batch * sizeof ( cufftComplex ) ) );
    // Allocate the fresnel prop
    cufftComplex* frProp;
    cuda_assert ( cudaMalloc ( ( void** ) &frProp, size_single * sizeof ( cufftComplex ) ) );
    // Allocate a temporary derivative array
    cufftComplex* dEdVTemp;
    cuda_assert ( cudaMalloc ( ( void** ) &dEdVTemp, size_batch * sizeof ( cufftComplex ) ) );
    // Derivative of the intensity, i.e. multiply with complex conjugate of psi
    undoIntensity ( dEdV, psi, params, params_d, (params->IM.Slices + 1));  //Make better implementation of this!
    // Derivative of the Far Field Transform
    undofarFieldTransform(dEdV, params, params_d);
    // Construct Fresnel propagator
    fresnelPropagator ( frProp, params, params_d, -1 );
    // Do the backward propagation through the specimen
    copyPsiBatchInOut <<< dEdV->gS, dEdV->bS >>> ( dEdVTemp, dEdV->values_d, params_d, params->IM.Slices, 1 );
    cuda_assert ( cudaDeviceSynchronize () );

    for ( int z = ( params->IM.Slices - 1 ); z > -1 ; z-- )
    {
        z_batch = z * size_batch;
        // Copy potential
        copyBatchInOut<<<V->gS, V->bS>>> ( t, V->values_d, params_d, z, 1);
        cuda_assert ( cudaDeviceSynchronize () );
	// Convert potential to transmission function
        potential2Transmission <<< params->CU.gSB, params->CU.bS >>> ( t, t, size_batch ); 
        cuda_assert ( cudaDeviceSynchronize () );
	// Propagation to next slice
        convolveWithFrProp (dEdVTemp, frProp, params, params_d); 
        // The derivative w.r.t. the potential
        copyPsiBatchInOut <<<dEdV->gS, dEdV->bS>>> ( dEdV->values_d, dEdVTemp, params_d, z, -1 );
        cuda_assert ( cudaDeviceSynchronize () );
        probeObjectInteraction<<<dEdV->gS, dEdV->bS>>> ( dEdV->values_d, psi->values_d, params_d, z, 0 );
        cuda_assert ( cudaDeviceSynchronize () );
        bandwidthLimit( &(dEdV->values_d[z_batch]), params );
        probeObjectInteraction<<<dEdV->gS, dEdV->bS>>> ( dEdV->values_d, t, params_d, z, -1 );
        cuda_assert ( cudaDeviceSynchronize () );

        // The rest of the back propagation
        if ( z > 0 )
        {
            bandwidthLimit( t, params );
            multiplyElementwise <<< params->CU.gSB, params->CU.bS >>> ( dEdVTemp, t, params_d, 1);
            cuda_assert ( cudaDeviceSynchronize () );
        }
        if ( z == 0 )
        {
            bandwidthLimit( t, params );
            multiplyElementwise <<< params->CU.gSB, params->CU.bS >>> ( dEdVTemp, t, params_d, 1);
            cuda_assert ( cudaDeviceSynchronize () );
            cublas_assert(cublasCcopy(params->CU.cublasHandle, size_batch, dEdVTemp, 1, dpsi_t->values_d, 1));
            complexConjugate <<< params->CU.gSB, params->CU.bS >>> ( dpsi_t->values_d, size_batch );
            cuda_assert( cudaDeviceSynchronize () );
        }
    }

    cuda_assert ( cudaFree ( frProp ) );
    cuda_assert ( cudaFree ( t ) );
    cuda_assert ( cudaFree ( dEdVTemp ) );
}

void bandwidthLimit ( cufftComplex* f, params_t* params )
{
    cufft_assert ( cufftExecC2C ( params->CU.cufftPlan, f, f, CUFFT_FORWARD ) );
    cuda_assert ( cudaDeviceSynchronize () );
    zeroHighFreq <<< 2 * params->CU.gS2D, params->CU.bS >>> ( f, params->IM.ProbeDim, params->IM.ProbeDim);
    cuda_assert ( cudaDeviceSynchronize () );
    cufft_assert ( cufftExecC2C ( params->CU.cufftPlan, f, f, CUFFT_INVERSE ) );
    cuda_assert ( cudaDeviceSynchronize () );
    const int size = params->IM.ProbeDim * params->IM.ProbeDim;
    const float alpha = 1.f / ( ( float ) size );
    cublas_assert ( cublasCsscal ( params->CU.cublasHandle, size, &alpha, ( cuComplex* ) f, 1 ) );
    cuda_assert ( cudaDeviceSynchronize () );
}

void incomingWave(cufftComplex* psi, params_t* params, params_t* params_d)
{
   const int gS2D = params->CU.gS2D;
   const int bS = params->CU.bS;
   const int size = params->IM.ProbeDim * params->IM.ProbeDim;
   float alpha = 1.f;

    initialValuesComplex<<< gS2D, bS >>> (psi, size, 1.f, 0.f);
    cuda_assert(cudaDeviceSynchronize());
    multiplyLensFunction<<< gS2D, bS>>> (psi, params_d, 1);
    FFTShift<<< params->CU.gS2D, params->CU.bS >>>(psi, params->IM.ProbeDim, params->IM.ProbeDim, params->IM.batchSize);
    cufft_assert (cufftExecC2C(params->CU.cufftPlan, psi, psi, CUFFT_INVERSE));
    bandwidthLimit(psi, params);
    cublas_assert(cublasScnrm2(params->CU.cublasHandle, size, psi, 1, &alpha));
    alpha = 1.f / alpha;
    cublas_assert(cublasCsscal(params->CU.cublasHandle, size, &alpha, psi, 1));
}


void fresnelPropagator ( cufftComplex* frProp, params_t* params, params_t* params_d, int flag )
{
    // flag = 1 for forward propagation and -1 for backward propagation
    fresnelPropagatorDevice <<< params->CU.gS2D, params->CU.bS >>> ( frProp, params_d, flag );
    cuda_assert ( cudaDeviceSynchronize () );
    zeroHighFreq <<< 2 * params->CU.gS2D, params->CU.bS >>> ( frProp, params->IM.ProbeDim, params->IM.ProbeDim);
    cuda_assert ( cudaDeviceSynchronize () );
    //divide frProp with m1*m2, then the renormalization in the multislice later on does not need to be done explicitly anymore.
    const int size = params->IM.ProbeDim * params->IM.ProbeDim;
    const float alpha = 1.f / ( ( float ) size );
    cublas_assert ( cublasCsscal ( params->CU.cublasHandle, size, &alpha, ( cuComplex* ) frProp, 1 ) );
    cuda_assert ( cudaDeviceSynchronize () );
}

void convolveWithFrProp ( cufftComplex* t, cufftComplex* frProp, params_t* params, params_t* params_d)
{
    cufft_assert ( cufftExecC2C ( params->CU.cufftPlanMany, t, t, CUFFT_FORWARD ) );
    cuda_assert ( cudaDeviceSynchronize () );
    multiplyElementwise <<< params->CU.gSB, params->CU.bS >>> ( t, frProp, params_d, 0);
    cuda_assert ( cudaDeviceSynchronize () );
    cufft_assert ( cufftExecC2C ( params->CU.cufftPlanMany, t, t, CUFFT_INVERSE ) ); 
    cuda_assert ( cudaDeviceSynchronize () );
}

void farFieldTransform(Array* psi, params_t* params, params_t* params_d)
{
    const int size = params->IM.ProbeDim * params->IM.ProbeDim;
    const int size_batch = params->IM.ProbeDim * params->IM.ProbeDim * params->IM.batchSize;
    float alpha = sqrtf(1.f / ((float)size));

    cufftComplex* temp;
    cuda_assert ( cudaMalloc ( ( void** ) &temp, size_batch*sizeof ( cufftComplex ) ) );

    copyPsiBatchInOut<<< psi->gS, psi->bS >>> ( temp, psi->values_d, params_d, params->IM.Slices, 1 );
    cuda_assert(cudaDeviceSynchronize());
    FFTShift<<< params->CU.gSB, params->CU.bS >>>(temp, params->IM.ProbeDim, params->IM.ProbeDim, params->IM.batchSize);
    cuda_assert(cudaDeviceSynchronize());
    cufft_assert(cufftExecC2C(params->CU.cufftPlanMany, temp, temp, CUFFT_FORWARD));
    cuda_assert(cudaDeviceSynchronize());
    cublas_assert(cublasCsscal(params->CU.cublasHandle, size_batch, &alpha, (cuComplex*) temp, 1));
    cuda_assert(cudaDeviceSynchronize());
    copyPsiBatchInOut<<< psi->gS, psi->bS >>>  ( psi->values_d, temp, params_d, (params->IM.Slices + 1), -1 );
    cuda_assert(cudaDeviceSynchronize());


    cuda_assert(cudaDeviceSynchronize());
    cuda_assert(cudaFree(temp));
}

void undofarFieldTransform(Array* dEdV, params_t* params, params_t* params_d)
{
    const int size = params->IM.ProbeDim * params->IM.ProbeDim;
    const int size_batch = params->IM.ProbeDim * params->IM.ProbeDim * params->IM.batchSize;
    const float alpha = sqrtf(1.f / ((float)size));

    cufftComplex* temp;
    cuda_assert ( cudaMalloc ( ( void** ) &temp, size_batch*sizeof ( cufftComplex ) ) );
    copyPsiBatchInOut <<< dEdV->gS, dEdV->bS >>> ( temp, dEdV->values_d, params_d, (params->IM.Slices + 1), 1 );
    cuda_assert(cudaDeviceSynchronize());
    cufft_assert(cufftExecC2C(params->CU.cufftPlanMany, temp, temp, CUFFT_FORWARD ) ); //Inverse FFT not necessary
    cuda_assert ( cudaDeviceSynchronize () );
    FFTShift<<< params->CU.gSB, params->CU.bS >>> (temp, params->IM.ProbeDim, params->IM.ProbeDim, params->IM.batchSize);
    cublas_assert(cublasCsscal(params->CU.cublasHandle, size_batch, &alpha, (cuComplex*) temp, 1));
    cuda_assert(cudaDeviceSynchronize());
    copyPsiBatchInOut <<< dEdV->gS, dEdV->bS >>>  ( dEdV->values_d, temp, params_d, (params->IM.Slices), -1 );
    cuda_assert ( cudaDeviceSynchronize () );
    cuda_assert(cudaFree(temp));
}

void doIntensity ( Array* psi, params_t* params, params_t* params_d, int j)
{
    const int size_batch = params->IM.ProbeDim * params->IM.ProbeDim  * params->IM.batchSize;

    cufftComplex* temp;
    cuda_assert ( cudaMalloc ( ( void** ) &temp, size_batch*sizeof ( cufftComplex ) ) );
    copyPsiBatchInOut <<< psi->gS, psi->bS >>> ( temp, psi->values_d, params_d, j , 1);
    cuda_assert(cudaDeviceSynchronize());
    doIntensity_d <<< params->CU.gSB, params->CU.bS>>> ( temp, temp, size_batch );
    cuda_assert ( cudaDeviceSynchronize () );
    copyPsiBatchInOut<<< psi->gS, psi->bS >>>  ( psi->values_d, temp, params_d, j + 1, -1 );
    cuda_assert(cudaDeviceSynchronize());
    cuda_assert(cudaFree(temp));
}

void undoIntensity ( Array* dEdV, Array* psi, params_t* params, params_t* params_d, int j )
{
    undoIntensity_d<<< dEdV->gS, dEdV->bS >>> ( dEdV->values_d, psi->values_d, params_d, j);
    cuda_assert ( cudaDeviceSynchronize () );
}

void saveTestImageComplex( char* fileName, cufftComplex* src, int size )
{
    cufftComplex* tomp;
    tomp = ( cufftComplex* ) malloc ( size * sizeof ( cufftComplex ) );
    cuda_assert ( cudaMemcpy ( tomp, src, size * sizeof ( cufftComplex ), cudaMemcpyDeviceToHost ) );

    float* tomp2;
    tomp2 = ( float* ) malloc ( size * sizeof ( float ) );

    char* fileName2;
    fileName2 = ( char* ) malloc ( 128 * sizeof ( char ) );

    for ( int el = 0; el <  size; el++ )
    { tomp2[el] = tomp[el].x; }

    sprintf ( fileName2, "%s_re.bin", fileName );
    writeBinary ( fileName2, tomp2, size );

    for ( int el = 0; el <  size; el++ )
    { tomp2[el] = tomp[el].y; }

    sprintf ( fileName2, "%s_im.bin", fileName );
    writeBinary ( fileName2, tomp2, size );

    for ( int el = 0; el <  size; el++ )
    { tomp2[el] = tomp[el].x * tomp[el].x + tomp[el].y * tomp[el].y; }

    sprintf ( fileName2, "%s_I.bin", fileName );
    writeBinary ( fileName2, tomp2, size );

    free ( fileName2 );
    free ( tomp );
    free ( tomp2 );
}

void saveTestImageFloat (std::string fileName, float* src, int size )
{
    float* tomp;
    tomp = ( float* ) malloc ( size * sizeof ( float ) );
    cuda_assert ( cudaMemcpy ( tomp, src, size * sizeof ( float ), cudaMemcpyDeviceToHost ) );

    fileName.append("_I.bin");

    std::ofstream ofs(fileName.c_str(),  std::ios::binary);
    ofs.write((char*) &tomp[0], size * sizeof(float));

    free ( tomp );
}

