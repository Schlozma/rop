/*------------------------------------------------
	Copyright (C) 2019 Marcel Schloz and Wouter Van den Broek
	See ROP.cu for full notice
------------------------------------------------*/

#ifndef mathematics_H
#define mathematics_H

#include <cufft.h>
#include <cuComplex.h>
#include "cuda_assert.hpp"
#include "initialization.h"
#include "float.h"

__global__ void reduce(float* fOut, cufftComplex* fIn, int size, int flag);

__global__ void complexConjugate(cufftComplex* src, int size);

__global__ void divideBy ( float* f1, float* f2, int size );

__global__ void addEpsilon ( float* f, int size );

__global__ void logValues ( float* f1, float* f2, int size );

__global__ void signValues ( float* f, int size );

__global__ void sumValues(cufftComplex* temp, cufftComplex* V, params_t* params);

__global__ void copySingle2Double ( double* fD, float* fS,  int size );

__global__ void copyIn ( cuComplex* psi, cufftComplex* temp, params_t* params, int j );

__global__ void scaleObject ( cufftComplex* Object, float norm, params_t* params );

__global__ void copyObjectIn ( cufftComplex* ObjectOutx, cufftComplex* ObjectOuty, cufftComplex* ObjectIn, params_t* params );

__global__ void copyObjectOut ( cufftComplex* ObjectOut, cufftComplex* ObjectInx, cufftComplex* ObjectIny, params_t* params );

__global__ void addObjectOut ( cufftComplex* ObjectOut, cufftComplex* ObjectInx, cufftComplex* ObjectIny, float fctr, params_t* params );

__global__ void cropIn ( cuComplex* psi, cuComplex* psi_, params_t* params, params_t* params_);

__global__ void addIn ( cuComplex* psi, cufftComplex* temp, params_t* params, int j );

__global__ void multiplyElementwise(cufftComplex* f0, cufftComplex* f1, params_t* params, int flag );

__global__ void multiplyPosDerivatives(cufftComplex* f0, cufftComplex* fx, cufftComplex* fy, int m1, int m2, int batchSize);

__global__ void probeObjectInteraction ( cuComplex* f0, cufftComplex* f1, params_t* params, int j, int flag );

__global__ void FFTShift(cufftComplex* f0, int m1, int m2, int batch);

__global__ void doIntensity_d(cufftComplex* I_d, cufftComplex* psi, int size);

__global__ void undoIntensity_d ( cufftComplex* f0, cufftComplex* f1, params_t* params, int j );

__global__ void copyPsiBatchInOut ( cufftComplex* f0, cuComplex* f1, params_t* params, int j, int flag );

__global__ void copyBatchInOut ( cufftComplex* f0, cufftComplex* f1, params_t* params, int j, int flag );

__global__ void initialValuesComplex( cuComplex* V, int size, float initRe, float initIm );

__global__ void initialValuesFloat( float* V, int size, float init);

__global__ void initialValuesArray ( cufftComplex* fin, int dimX, int dimY, int dimZ);

__global__ void sumValues_helper( cuComplex* dst, cuComplex* src, cuComplex thold, int sizeSrc, const int flag, const float mu0 );

__global__ void addDerivativeAbsValues(cuComplex* dst, cuComplex* src, float fctr, int size, float mu0 );

__global__ void createWedgeDerivative(cufftComplex* Vkx, cufftComplex* Vky, float* W, params_t* params);

__global__ void batchedCaxpy( cuComplex* f0, cufftComplex* f1, float beta, params_t* params, int j, int flag);

__device__ float absValue( float x, const float mu0 );

void realPart(float* Vri, cufftComplex* V, int size);

void imagPart(float* Vri, cufftComplex* V, int size);

float sumAbsValues(cuComplex* V, const int size, const float mu0 );

cuComplex sumAbsValues_helper( cuComplex* V, cuComplex thold, const int size, const int flag, const float mu0 );

float sumWedgeAbsValues(cufftComplex* Vk, float* W, const int size);

cuComplex sumWedgeAbsValues_helper(cufftComplex* Vk, float* W, cuComplex thold, const int size);

__global__ void fillIn(cufftComplex* V, params_t* params_d);

#endif
